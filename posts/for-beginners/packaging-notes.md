<html><body><p>I've done easy fixes (debdiffs) in Ubuntu and find I need to look up exactly how I want to do a debdiff every time.   Last time I had to look at 5 different docs to get all the commands I needed.   The bug I based this on was a debian only change (Init script), I plan to update it next time I have an actual source change.
</p><ol>
 	<li>Start a new VM/ Cloud instance</li>
 	<li>sudo apt-get install packaging-dev</li>
 	<li> apt-get source &lt;package_name&gt;  ;  apt-get build-dep &lt;package_name&gt;</li>
 	<li>cd into-directory-created</li>
 	<li>Make the change (if it's only a debian/ change)</li>
 	<li>dch -i   (document it)</li>
 	<li>debuild -S -us -uc  (build it)</li>
 	<li>debdiff rrdtool_1.4.7-1.dsc rrdtool_1.4.7-1ubuntu1.dsc &gt; rrdtool_1.4.7-1ubuntu1.debdiff   (make the debdiff - note to me, change the name later)</li>
 	<li>cd into-directory; DEB_BUILD_OPTIONS='nostrip noopt debug' fakeroot debian/rules binary  (build it)</li>
 	<li> Test it</li>
</ol>
Docs used:
<ol>
 	<li>http://packaging.ubuntu.com/html/traditional-packaging.html</li>
 	<li>http://packaging.ubuntu.com/html/fixing-a-bug-example.html</li>
 	<li>http://cheesehead-techblog.blogspot.com/2008/10/creating-patch-to-fix-ubuntu-bug.html</li>
 	<li>https://wiki.debian.org/IntroDebianPackaging</li>
 	<li>https://wiki.debian.org/BuildingTutorial</li>
</ol>
Comments:<strong>toobuntu</strong>

My understanding is the current best practice is to use mk-build-deps, provided by the devscripts package, to install build dependencies. See https://wiki.debian.org/BuildingAPackage#Get_the_build_dependencies

With apt-get build-dep, the installed packages are marked as manually installed and so won't be offered for autoremoval. mk-build-deps creates a dummy metapackage with the build deps as dependencies. When that generated package is later removed, so are the build deps.

Or, add "APT::Get::Build-Dep-Automatic true;" to your apt.conf to mark the build deps as automatically installed so they will be removed with the next "apt-get autoremove"</body></html>