<html><body><p>&gt;<span style="font-weight: bold;">Why do you like <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> and <a href="http://www.ubuntu.com/">Ubuntu</a>?</span>

My Answer:
<span style="font-weight: bold;">I consider software important. Really really important. Why?</span>
Most people don't really understand just how much power <span style="font-style: italic;">software </span>has over our current society. Let's pretend all the software in the world was really just one single person. What could that person do?
</p><ul>
	<li>Change everyone's bank account balances (and change local records to match so noone notices!)</li>
	<li>Choose many elected officials</li>
	<li>Isolate individuals socially, make their emails go unanswered, etc (or worse make seem to send emails creating arguments)</li>
	<li>In some places, arrest people</li>
	<li>Ruin a credit score</li>
	<li>Change the news</li>
	<li>Change history</li>
	<li>Nuke the world (or just provide bad information to leaders and let them do it)</li>
</ul>
Yes, quite skynetish, eh? Luckily software isn't just one individual, nor was it written by one person. But the <span style="font-weight: bold;">power</span> is still held by all of the software in the world (or really the people who created it).

<span style="font-weight: bold;">I believe that an essential part of democracy is allowing those that are interested to have a hand in controlling the systems that have the power. How?</span>

The <a href="http://www.gnu.org/licenses/licenses.html">(A)GPL</a> ensure <span style="font-weight: bold;">perpetually</span> that the end user of the software will have the clear ability to take control of the software on their devices. The AGPL could also be used to force transparency, another democratic necessity, of the methods used in voting, banking, etc.

<span style="font-weight: bold;">A world with just Free Software (let's just say all AGPL for simplicity) should be substantially closer to <a href="http://en.wikipedia.org/wiki/Perfect_competition">perfect competition</a>. Why?</span>
<ul>
	<li>It lowers the <span style="font-weight: bold;">barrier to entry</span>, you can just pick up the source code to Gmail and make a new product. (Which Gmail can then copy from you).</li>
	<li><span style="font-weight: bold;">More sellers</span>. The more sellers, the more responsive a supplier needs to be to you cause you can switch.</li>
	<li><span style="font-weight: bold;">Similar Products.</span> They are mostly compatible with their specific value adds that will be likely incorporated into the next version of a competitor.</li>
</ul>
Perhaps this sounds a lot like Linux distros to you? :)

<span style="font-weight: bold;">Well, Why Ubuntu?</span>
<span style="font-style: italic;">We aren't there yet</span>
Ubuntu makes some of the necessary compromises (proprietary drivers, etc) that give many more people, more of the freedom and control then they would have had without it. Right now, I feel Ubuntu has the best chance of getting us closer.

<span style="font-style: italic;">Community</span>
People need help to get involved with politics; communities exist to help concerned citizens to help change their governments. The Ubuntu community is here to help new users and contributors get involved helping to shape their software. Which is critical, because even if you have the freedom or right, doesn't mean you have the knowledge to use it. Receptive communities, like Ubuntu's, help get the knowledge to you.

<span style="font-weight: bold;">Where are you going with this?</span>
I was actually trying to answer the question, Why do I hate Microsoft? (I mean, come on, I don't want Mono included on the default Ubuntu install, so I clearly hate Microsoft or Novell, or somebody)

<span style="font-weight: bold;">I don't hate Microsoft. I don't consider myself anti Microsoft or anti Mono. </span>I have actually set people up to use both Windows and Mono applications before, gasp! And I actually like Novell. (I'm also a MCSA)

I consider myself pro Free Software and pro community created and governed languages (as well as content, and much more).

We have a better way to create software, a more democratic control structure over what we do, and most importantly, we give users great software and a path (through the community) to help them be in control of their software and have a say in its future.

Related Posts, somewhat referenced<span style="font-weight: bold;">:
</span><span style="font-family: Georgia, Times New Roman; font-size: 100%; color: #000000;"><span style="font-family: Georgia, Times New Roman; font-size: 100%; color: #000000;">SciAm Column on "Rational Atheism"; The Dangers of Being "Anti" Rather Than "Pro"
</span></span>Doctor Mo previously had a post called "Why do you like Microsoft"</body></html>