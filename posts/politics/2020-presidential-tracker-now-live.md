<!--
.. title: 2020 Presidential Tracker now live
.. slug: 2020-presidential-tracker-now-live
.. date: 2019-05-16 00:29:04 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

# May Update
I've revamped it by creating a single score to summarize all the tests - the goal is to have some useful predictivate quality - and be easier to track over time. To predict how prepared they are for a "bump" or their "monent", but also provide an idea of how much outside actors might be able to meddle with their campaign.

There are 3 different categories:

 * Performance metrics - The top four are donaldjtrump.com, julianforthefuture.com, joebiden.com, and amyklobuchar.com.	
 * Email security  - Top are elizabethwarren.com, hickenlooper.com, corybooker.com, and joebiden.com.  They will have a much better time communicating to their supporters.
 * and other website security metrics

Today's results:

 * 65 - joebiden.com -  the highest score. only one with a non-F letter grade, but it's still a D.
 * 55,56 - corybooker.com, hickenlooper.com - both substancially improved over last evaluation.
 * 48,49 - amyklobuchar.com, elizabethwarren.com
 * Low 40s - betoorourke.com, , michaelbennet.com, johndelaney.com
 * High 30s - jayinslee.com, marianne2020.com, berniesanders.com, stevebullock.com
 * Low 30s - donaldjtrump.com, ericswalwell.com, weld2020.org
 * 20s - tulsigabbard.org, kirstengillibrand.com, kamalaharris.org, billdeblasio.com, peteforamerica.com, wayneforamerica.com, timryanforamerica.com
 * 18 - sethmoulton.com
 * 9 - yang2020.com - Will the math lover stand being last?

All the candidates can definitely do better, my website gets a 79. I don't really see anyone getting out of the race before the first debate, so I can't make any clear predictions at this point.

You can get the full details at the [2020 presidential website tracker](../../pages/papers/2020-presidential-websites-tracker.html). .

---
# Original post:

Now it's a tradition (did it in [2016 too](/posts/politics/presidential-candidate-website-survey-update.html))... I'm launching my [2020 presidential website tracker](../../pages/papers/2020-presidential-websites-tracker.html). 

I'm being harsh and limiting everyone to a C rating for now. There are some basic things all the candidates really could be doing. If you see a mistake I made or find something new to track, feel free to report a [bug/pull request](https://gitlab.com/BryanQuigley/bryanquigley.com/tree/master/pages/papers).
