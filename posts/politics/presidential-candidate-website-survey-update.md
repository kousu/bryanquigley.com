<html><body><p>The race is now down to 5. (From 21!)

What's changed in their website setups?

<strong>Donald Trump</strong> got rid of Flash, otherwise everything else appears to be the same.

<strong>Ted Cruz</strong> went from a A+ rating to just an A (lost HSTS?).

Nothing changed for <strong>John Kasich</strong>.

<strong>Hillary Clinton</strong> went from an inconsistent server setup with many IPv4 addresses to just 1 IPV4 address.   The www. redirect behavior (from without to it) does mess up HTTPS Everywhere and ssllabs tests.     A major plus is she added HSTS to her site, so her ssllabs rating is now A+.

<strong>Bernie Sanders</strong> added IPv6 support and HSTS to the main site.  Unfortunately a sha2 intermediate certificate prevents his site from going from A to A+.  And his donation provider has HSTS setup correctly and get's an A+.

At this point in the campaign, only A ratings (ssllabs) are left!  The Democrats seem to have prioritized implementing HSTS, but neither appears to have gone for the <a href="https://www.chromium.org/hsts">preload list</a>.

<a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security">HSTS</a> - Means you tell the browser to enforce SSL

<a href="/wp-content/uploads/2016/03/presedentialwebsites_last5.ods" rel="">You can find the raw data in this spreadsheet</a>

I also included sub domains in this list, but it wasn't as interesting as I hoped.</p></body></html>
