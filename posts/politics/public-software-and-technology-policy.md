<html><body><p>&gt;
</p><div style="text-align: left;">Late last year I ran with the Cherry Hill Linux User Group a Health Care IT Community Discussion at our local library and we created a <a href="http://groups.google.com/group/chlug/browse_thread/thread/57a4d57ea41e036e?pli=1">report</a> and submitted it to the Obama Administration (they asked for it) with the hope that they will implement some of our ideas.

Summary:
This evening we focused on Health Care IT, referred to in many debates as the magic bullet that will make health care affordable again. We agree that IT can and must play a major role in Health Care Reform, but could be a major hindrance if we make the wrong choices.
The Federal Government through the Department of Health and Human Services (DHHS) should:
• Maintain a fully free and open source electronic medical record (EMR) system.
• Mandate the EMR be taught in all medical and nursing schools.
• Mandate an open and freely implementable EMR communication standard.
• Mandate a national medical identiﬁcation number and prohibit the use of, and storage of, Social Security Numbers in any health care system.

It has gotten me thinking about other ways the Federal government can help other local systems such as schools, townships, maybe even police and fire stations. I'm going to use Education as the best other example:
<ol>
 	<li>Federal goverment funds development of free linux distribution for schools. Requires use of GPL for everything it pays for to ensure school software remains free. This is designed to be the end-all for schools, (maybe Moodle is at the core and all systems authenticate to it?). Basically they pay to add features to make these GPL-licensed systems do everything a school needs.</li>
 	<li>Federal government launches grant program to implement free linux distribution in 5 schools (made up number) in every state with the condition that the schools will allow other schools technology departments in to look around and help train them.</li>
 	<li>Profit, err less profit as every school no longer needs to pay for any licenses for software. It also encourages a local software ecosystem as schools can pay locally to fix a bug or add a feature.</li>
</ol>
Perhaps these should both be added to the stimulus package? I'd say this would get a great ROI compared to some of the other stuff on there.

Why GPL? Because I want the federally funded systems for education and health care and any modifications made to them to always be free to hospitals and schools. They are public institutions and as a matter of good public policy we should not pay for the same work over and over and over again, especially if the federal government already paid for it.</div>
<div></div>
<div><strong>Comments</strong></div>
<div>
<ul>
 	<li id="comment-1778" class="comment even thread-even depth-1"><article id="div-comment-1778" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">ScottK</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-02-14T18:49:00+00:00"> February 14, 2009 at 6:49 pm </time></div>
</footer>
<div class="comment-content">

&gt;Actually works of the US federal government are public domain. So as long as any contracts are written correctly so that data rights are purchased as part of the efforts, it would be public domain.

</div>
</article></li>
 	<li id="comment-1779" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1779" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">dsas</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-02-15T10:21:00+00:00"> February 15, 2009 at 10:21 am </time></div>
</footer>
<div class="comment-content">

&gt;In Britain we are implementing a nationwide medical IT system. So far it’s a catastrophe, as far as I know no savings have materialised, just costs. I think all of your points except the second have been part of our system.

Our system is the NHS Care Records Service, I haven’t read your report yet so I don’t know if you’ve looked into it.

P.S. Schools pay near zero for OS and office licences from microsoft, there may be substantial savings to be made in other areas though.

@ScottK There’s a difference between works of and works for the government as far as I know.

</div>
</article></li>
 	<li id="comment-1780" class="comment even thread-even depth-1"><article id="div-comment-1780" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">gQuigs</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-02-16T00:39:00+00:00"> February 16, 2009 at 12:39 am </time></div>
</footer>
<div class="comment-content">

&gt;@ScottK I agree with the distinction dsas made.

@dsas We did discuss at the meeting both the British system (which has been a disaster) and the Canadian (which has been a success).

My local school district pays 300 to 400K a year for MS Software Assurance. (There total budget is in the 150 million range)

</div>
</article></li>
</ul>
</div></body></html>