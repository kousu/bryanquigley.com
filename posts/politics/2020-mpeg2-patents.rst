<!--
.. title: 3 Malaysia MPEG-2 Patents left
.. slug: 3-Malaysia-MPEG-2-Patents-left
.. date: 2020-02-17 00:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

.. _appear: https://www.mpegla.com/programs/mpeg-2/patent-list/
.. _US5565923A: https://patents.google.com/patent/US5565923A/en
.. _tracker: /pages/mpeg2-patent-tracker.html

With February 13th passing it would appear_ there are only 3 Malaysia patents left:

 * MY 128994 (possible expiration of 30 Mar 2022)
 * MY 141626-A (possible expiration of 31 May 2025)
 * MY-163465-A (possible expiration of 15 Sep 2032)

These two just expired:

 * MY 118734-A - Exp. Jan 31, 2020
 * PH 1-1995-50216 - Exp. Feb 13, 2020

I am very much not a patent lawyer, but my reading indicates all the 3 remaining are really all the same expired US Patent US5565923A_ with varying Grant dates causing to expire far in the future.

I've started a detailed tracker_  for those who want more details.
