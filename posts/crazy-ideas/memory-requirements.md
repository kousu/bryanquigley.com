<html><body><p>Mostly kicked off by this <a href="https://web.archive.org/web/20161113201500/http://doctormo.org/2009/10/22/ubuntus-minimum-requirements/">post (Dead, now wayback machine)</a>

<style type="text/css"><!--

--></style>
</p><table width="100%" border="1" cellspacing="0" cellpadding="4"><colgroup><col width="85*"> <col width="171*"></colgroup>
<tbody>
<tr valign="TOP">
<td width="33%">OS</td>
<td width="67%">Required / Realistic</td>
</tr>
<tr valign="TOP">
<td width="33%">Ubuntu (full Gnome)</td>
<td width="67%">384 MB / 512 MB</td>
</tr>
<tr valign="TOP">
<td width="33%">Xubuntu</td>
<td width="67%">192 MB / 256 MB</td>
</tr>
<tr valign="TOP">
<td style="font-weight: bold;" width="33%">Windows XP</td>
<td style="font-weight: bold;" width="67%">64 MB / 128 MB</td>
</tr>
<tr valign="TOP">
<td width="33%">Windows Vista Home Basic</td>
<td width="67%">512 MB</td>
</tr>
<tr valign="TOP">
<td width="33%">Windows Vista (Other)</td>
<td width="67%">1 GB</td>
</tr>
<tr valign="TOP">
<td width="33%">Windows 7 32 bit</td>
<td width="67%">1 GB</td>
</tr>
<tr valign="TOP">
<td width="33%">Windows 7 64 bit</td>
<td width="67%">2 GB</td>
</tr>
</tbody>
</table>
Ubuntu is approaching Windows Vista Home's minimum memory specs, but is still a long way off our biggest competitor, Windows XP (70% market share and our only real competitor in netbooks). With netbooks usually having 512 - 1 GB of memory, it seems like XP would really let the user run many more applications (yes I am ignoring anti-virus and all the other random stuff OEMs load onto Windows to make it slower). So, I just have one question:

How hard would it be to reduce Ubuntu's memory usage from 9.10 to 10.04 by <i>just</i> 64 MB (oh, and does anyone want to make this an official goal for 10.04)?

I have knowledge of at least one school district where the majority of computers have only 128 MB of RAM. They are running XP and want to switch to Linux, but it was simply not an option due to memory. (And no if they don't have a big IT budget, read: no budget for LTSP)

Win 7 requirements <a title="Linkification: http://www.microsoft.com/windows/windows-7/get/system-requirements.aspx" href="http://www.microsoft.com/windows/windows-7/get/system-requirements.aspx">http://www.microsoft.com/windows/windows-7/get/system-requirements.aspx</a>
Win xp requirements <a title="Linkification: http://support.microsoft.com/kb/314865" href="http://support.microsoft.com/kb/314865">http://support.microsoft.com/kb/314865</a>
Ubuntu requirements <a title="Linkification: https://help.ubuntu.com/community/Installation/SystemRequirements" href="https://help.ubuntu.com/community/Installation/SystemRequirements">https://help.ubuntu.com/community/Installation/SystemRequirements</a>
Win Vista requirements <a title="Linkification: http://www.microsoft.com/windows/windows-vista/get/system-requirements.aspx" href="http://www.microsoft.com/windows/windows-vista/get/system-requirements.aspx">http://www.microsoft.com/windows/windows-vista/get/system-requirements.aspx</a>

<style type="text/css"><!--Ubuntu is approaching Windows Vista Home's minimum memory specs, but is still a long way off our biggest competitor, Windows XP (70% market share and our only real competitor in netbooks). With netbooks usually having 512 - 1 GB of memory, it seems like XP would really let the user run many more applications (yes I am ignoring anti-virus and all the other random stuff OEMs load onto Windows to make it slower). So, I just have one question:

How hard would it be to reduce Ubuntu's memory usage from 9.10 to 10.04 by <i>just 64 MB (oh, and does anyone want to make this an official goal for 10.04)?

I have knowledge of at least one school district where the majority of computers have only 128 MB of RAM. They are running XP and want to switch to Linux, but it was simply not an option due to memory. (And no if they don't have a big IT budget, read: no budget for LTSP)

<span style="font-size:85%;">Win 7 requirements http://www.microsoft.com/windows/windows-7/get/system-requirements.aspx
Win xp requirements http://support.microsoft.com/kb/314865
Ubuntu requirements https://help.ubuntu.com/community/Installation/SystemRequirements
Win Vista requirements http://www.microsoft.com/windows/windows-vista/get/system-requirements.aspx</span>

--></style></body></html>
