<html><body><h2>Upstream Stable Cadence</h2>
We track upstreams, what they consider stable, we consider stable, we ship in rolling release under *conditions.
<h2>
Upstream Beta Cadence</h2>
We track upstreams beta/rc repo, what they consider "almost stable", we consider worth getting extremely easy user testing.  We try to ship in Rolling Release archive, but with different package names.  This will not work for everything.   Some upstreams don't really have RC/beta releases, others might be too complicated to keep in repo.

For example, meta-packages for:  firefox-beta, linux-rcs, etc.
Goal would be to evaluate doing this with packages in main only.  Others, like all of Gnome, might need a different plan.
<h2>Data.  Lots of data.</h2>
We need data that we can get an idea of how stable a product is. Some combination of reported bugs, automatic bugs, weighted in different ways.
The data should be generated from both the beta  and stable releases.

We also need to better connect users of the beta releases to upstream, faster, when they have a problem.
<h2>"Limited Delta Buffer" (*conditions)</h2>
First - Big changes to stable release separated by 3 weeks initially.  New Kernel and new Xorg both want to get in?  Only 1 get's in every 3 weeks.  In this case I would suggest letting the kernel go first, then three weeks later Xorg get's in.   This limits the volatility of the rolling release by limiting updates from all coming at the same time.
Second - Data driven modifications to buffering time, who get's in first, and what blocks what.    For a new kernel the data would come from that exact kernel in the beta package,   If we find that the kernel never breaks anything and is super stable, move it to just a one week buffer.
Third - refine if we consider upstream to be "wrong".  Maybe Linux kernel rc3 and above is all we ship in beta package, etc.

Rationale: We need a new concept of a stable system for a Rolling Release.  It should take advantage of and contribute to upstreams own stabilization procedures (their "beta cadence") while limiting the rate of change (delta) users of the rolling release need to deal with.   As for using 'Months', they are way to arbitrary.  Some months there will be nothing interesting, others we could have a new kernel, Xorg, Unity, and Gnome.

Obviously, this isn't perfect and needs a bunch more refinement, I just wanted to get this out there before March 18th (the deadline for proposals).  Thanks for reading and please let me know what you think.

 </body></html>