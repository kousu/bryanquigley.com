<html><body><p>Results from my <a title="Running Ubuntu/Unity on a machine that is not 64 bit capable?" href="http://bryanquigley.com/crazy-ideas/running-ubuntuunity-on-a-machine-that-is-not-64-bit-capable">survey</a> on 64 bit vs 32 bit usage on Unity.    The <a href="https://docs.google.com/spreadsheet/ccc?key=0AtPdLrdrtm1wdGozbi1oUTlFR1RSaDBKalRlQ0ptUUE&amp;usp=sharing">raw survey</a> results are also available.  Feel free to do your own analysis.

Format:  # of machines - information about them..
</p><h1>Out of the 32 bit only machines:</h1>
4 - that are likely 32 bit only, but didn't provide enough information to confirm
1 - doesn't work so well, old savage card, 700 MB of RAM, Ubuntu 12.04 (so 2d only)

*Remaining 14 32 bit only machines:
5 - with 1 GB of ram
9 - with 1.5 GB of ram or more

Ubuntu Version:
6 - 13.04
1 - 12.10
7 - 12.04

Processor:
7 - Intel Atom

From comments, 3 of these 32 bit only users find Unity slow, but usable. (All three are Atom N270/N280)
<h1>64 bit capable but:</h1>
2 - that should be 64 capable (and the users know this), but it doesn't work..  If this is you, please file a bug!
2 - have 64 bit capable machines (and 2GB of ram) and don't actually know it.
5 - have 64 bit capable machines, but run 32 bit Ubuntu (1 with 1 GB of RAM who mentions that the dash is slow)
<h1>Other scenarios:</h1>
1 - machine stuck at 12.04 because 12.10 requires PAE (is this true?)
1 - Parallels VM that has better performance on 32 bit from user
2 - that actually use other desktop environments cause they find Unity unusable ("Unity is unuseable because i only have 1 GIG Ram")
<h1>Not particularly relevant:</h1>
1 - let's not discuss this again...
1  - armv7 :)
9 - users who have 64 bit machines that work fine and responded anyway..
1 - running Unity on servers...
<h1>Conclusion?</h1>
For some reason whenever I do a survey I expect that the results will clearly paint the way to go forward.   They almost never do, but they can be used to start discussions.  I'm particularly concerned about the 2 users with 64 bit capable machines which they can't get to run 64 bit.

Some people have 64 bit machines with low RAM and would also be better served with a lighter option.  Others have quite beefy 32 bit machines with 4GB of RAM and a nice video card that can rock Unity.   Also some motherboard manufacturers disabled 64 bit support even though the processor supported it...  awesome.

To default to 32 or 64 bit when downloading Ubuntu?

Given the data, I think we can make a better exception case for 32 bit now..  Right now,  it says:
<em>"If you have a PC with the Windows 8 logo or UEFI firmware, choose the 64-bit download. <a href="https://help.ubuntu.com/community/UEFI">Read more </a>" </em>
I think it's easier for most people to see and understand:
<em>"If you have a 5+ year old PC,  a 3+ year old netbook, or 1 GB of ram choose the 32 bit version." *</em>

Thoughts?

* Theoretically we could even provide instructions for our users to figure this out.  Add a "Not sure?" in that case.</body></html>