<html><body><p>So.. <span id="SPELLING_ERROR_0" class="blsp-spelling-error">Memtest</span> tells you have bad ram! Here are your easy options:
</p><ol>
 	<li>Buy new ram</li>
 	<li>Turn off everything after the bad memory* (<span id="SPELLING_ERROR_1" class="blsp-spelling-error">mem</span>=###M option)</li>
 	<li>Turn off just the memory around the bad memory* (<span id="SPELLING_ERROR_2" class="blsp-spelling-error">memmap</span>=#M$###M option)</li>
</ol>
*May require moving RAM around in the computer for best results.

<span style="font-weight: bold;">Testing The Memory</span>
<span style="font-style: italic;">Tool 1: <span id="SPELLING_ERROR_3" class="blsp-spelling-error">Memtest</span></span>
There is the classic <span id="SPELLING_ERROR_4" class="blsp-spelling-error">Memtest</span> which is on every <span id="SPELLING_ERROR_5" class="blsp-spelling-error">Ubuntu</span> <span id="SPELLING_ERROR_6" class="blsp-spelling-error">LiveCD</span> and most other Free operating system <span id="SPELLING_ERROR_7" class="blsp-spelling-error">CDs</span>. It tests memory and gives you back fun technical numbers and also what sectors are bad in the easier form of 797M or 84M. It's the easier number we want. So try to write down the range (if applicable) of bad results.

<span style="font-style: italic;">Tool 2: </span><span id="SPELLING_ERROR_8" class="blsp-spelling-error" style="font-style: italic;">Memtester</span>
<span id="SPELLING_ERROR_9" class="blsp-spelling-error">Memtester</span> (<a href="apt:memtester">click here to install</a>) is like <span id="SPELLING_ERROR_10" class="blsp-spelling-error">memtest</span> only it runs from a command line once <span id="SPELLING_ERROR_11" class="blsp-spelling-error">Ubuntu</span> has loaded. It can't test your whole ram so you should make sure that it would be testing the area that <span id="SPELLING_ERROR_12" class="blsp-spelling-error">memtest</span> has detected being bad.
You absolutely need to run it with sudo/root.

<span style="color: #ff0000;">WARNING: Don' try to test your whole RAM, just say 100M over where the error is. Doing really close to your actual total ram will make your computer unresponsive (unless you are following this with nothing but a shell, then you might be fine).</span>

Ex. Let's say you have 150M taken up by your OS, the badram is at 797M (from memtest) and you have 1024M total memory.

This command would work:
<span id="SPELLING_ERROR_15" class="blsp-spelling-error"></span>
<blockquote><span id="SPELLING_ERROR_15" class="blsp-spelling-error">sudo</span> <span id="SPELLING_ERROR_16" class="blsp-spelling-error">memtester</span> 750M</blockquote>
<span style="font-weight: bold;">Option 1 - Buy New Ram</span>

Simple. Easy. Safest. Might be cheap if your computer supports new ram. Wastes hardware that we could likely get to work. Some people say that once RAM starts to go, it will just keep getting worse, I personally don't believe them.
I will not be liable for you losing data by using bad RAM. Use YOUR own judgment.

<span style="font-weight: bold;">Option 2 - Turn off everything after the bad memory (<span id="SPELLING_ERROR_18" class="blsp-spelling-error">mem</span>=###M option)</span>
This is simple take the lowest number from <span id="SPELLING_ERROR_19" class="blsp-spelling-error">memtest</span>, subtract let's say 3 from it for a bit of safety. Then add it to your kernel command line for <span id="SPELLING_ERROR_20" class="blsp-spelling-error">bootup</span>.
Ex. So for bad memory at 797M we would add to the default kernel options:
<span id="SPELLING_ERROR_21" class="blsp-spelling-error"></span>
<blockquote><span id="SPELLING_ERROR_21" class="blsp-spelling-error">mem</span>=795M</blockquote>
<span style="font-weight: bold;">Option 3 - Turn off just the memory around the bad memory (<span id="SPELLING_ERROR_23" class="blsp-spelling-error">memmap</span>=#M$###M option)
</span><span style="font-weight: bold; color: #ff0000;">WARNING</span><span style="color: #ff0000;">: </span><span style="font-weight: bold; color: #ff0000;">This is much less tested than Option 2. There may be bugs lurking about. </span>
This isn't much more difficult. Instead of just stopping at 795M, we are going to stop at 795M but then only ignore the next 10M. If there are multiple places try to make a range that includes them both.

Ex. So for bad memory at 804M and 806M we could use:
<blockquote><span id="SPELLING_ERROR_24" class="blsp-spelling-error">memmap</span>=10M$800M</blockquote>
But for this option I will fell better if you use <span id="SPELLING_ERROR_25" class="blsp-spelling-error">memtester</span> afterwards to make sure you actually got the right memory spot. (And maybe do it before too, too make sure you actually were testing the right spots in the test)

<span style="font-weight: bold;"><span id="SPELLING_ERROR_26" class="blsp-spelling-error">HowTo</span> Kernel Default Options</span>
Edit the file /boot/grub/menu.<span id="SPELLING_ERROR_27" class="blsp-spelling-error">lst</span> with <span id="SPELLING_ERROR_28" class="blsp-spelling-error">sudo</span>/root
(ex. <span id="SPELLING_ERROR_29" class="blsp-spelling-error">sudo</span> <span id="SPELLING_ERROR_30" class="blsp-spelling-error">nano</span> /boot/grub/menu.<span id="SPELLING_ERROR_31" class="blsp-spelling-error">lst</span>)\

Navigate to here
<blockquote>## ## Start Default Options ##
## default kernel options
## default kernel options for <span id="SPELLING_ERROR_32" class="blsp-spelling-error">automagic</span> boot options
## If you want special options for specific kernels use <span id="SPELLING_ERROR_33" class="blsp-spelling-error">kopt</span>_x_y_z
## where x.y.z is kernel version. Minor versions can be omitted.
## e.g. <span id="SPELLING_ERROR_34" class="blsp-spelling-error">kopt</span>=root=/<span id="SPELLING_ERROR_35" class="blsp-spelling-error">dev</span>/<span id="SPELLING_ERROR_36" class="blsp-spelling-error">hda</span>1 <span id="SPELLING_ERROR_37" class="blsp-spelling-error">ro</span>
## <span id="SPELLING_ERROR_38" class="blsp-spelling-error">kopt</span>_2_6_8=root=/<span id="SPELLING_ERROR_39" class="blsp-spelling-error">dev</span>/<span id="SPELLING_ERROR_40" class="blsp-spelling-error">hdc</span>1 <span id="SPELLING_ERROR_41" class="blsp-spelling-error">ro</span>
## <span id="SPELLING_ERROR_42" class="blsp-spelling-error">kopt</span>_2_6_8_2_686=root=/<span id="SPELLING_ERROR_43" class="blsp-spelling-error">dev</span>/<span id="SPELLING_ERROR_44" class="blsp-spelling-error">hdc</span>2 <span id="SPELLING_ERROR_45" class="blsp-spelling-error">ro</span>
# <span style="font-weight: bold;"><span id="SPELLING_ERROR_46" class="blsp-spelling-error">kopt</span>=root=<span id="SPELLING_ERROR_47" class="blsp-spelling-error">UUID</span>=f0906667-<span id="SPELLING_ERROR_48" class="blsp-spelling-error">bcde</span>-4b64-86<span id="SPELLING_ERROR_49" class="blsp-spelling-error">bc</span>-5a47320d5517 <span id="SPELLING_ERROR_50" class="blsp-spelling-error">ro</span></span></blockquote>
Edit the line (DO NOT GET RID OF THE #):
<blockquote># <span id="SPELLING_ERROR_51" class="blsp-spelling-error">kopt</span>=root=<span id="SPELLING_ERROR_52" class="blsp-spelling-error">UUID</span>=f0906667-<span id="SPELLING_ERROR_53" class="blsp-spelling-error">bcde</span>-4b64-86<span id="SPELLING_ERROR_54" class="blsp-spelling-error">bc</span>-5a47320d5517 <span id="SPELLING_ERROR_55" class="blsp-spelling-error">ro (add mem stuff here)</span></blockquote>
Then run:
<blockquote>sudo update-grub</blockquote>
Then reboot.

Thanks for following my <span id="SPELLING_ERROR_57" class="blsp-spelling-error">HowTo</span>. Many happy PCs and memory modules saved from landfills.

 

<strong>Comments</strong>

 

 
<ul>
 	<li id="comment-1940" class="comment even thread-even depth-1"><article id="div-comment-1940" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">LeDopore</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-08-07T16:46:51+00:00"> August 7, 2009 at 4:46 pm </time></div>
</footer>
<div class="comment-content">

&gt;Hey gQuig,

Thanks for the help! You have a well-written tutorial that's a beacon of hope for people with sick computers.

I have a quick question. When I try to work around my bad RAM, it looks like top still sees just as much RAM as before. Does that mean that my kernel isn't avoiding the bad ram?

My menu.lst file had the following line in it:

# kopt=root=UUID=a9f39ac2-c67f-418e-93ce-967bdda5e3a4 ro memmap=100M$1800M

and I did remember to sudo update-grub.

Thanks for your help!

LeDopore

</div>
</article></li>
 	<li id="comment-1942" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1942" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">gQuigs</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-08-07T20:43:21+00:00"> August 7, 2009 at 8:43 pm </time></div>
</footer>
<div class="comment-content">

&gt;@LeDopore: tell me more about your setup. Anything odd? It should definitely report a different amount.

Try looking at:
cat /proc/meminfo

When you are booting up, stop at grub, by pressing escape and try to edit the lines to see what is actually being used. Remove it and double check that it didn't change the amount of ram.

</div>
</article></li>
 	<li id="comment-1943" class="comment even thread-even depth-1"><article id="div-comment-1943" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">LeDopore</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-08-07T21:13:20+00:00"> August 7, 2009 at 9:13 pm </time></div>
</footer>
<div class="comment-content">

&gt;Thanks, gQuigs. I hadn't known about editing the boot options with grub.

I added the memmap=100M$1800M to grub right at boot and my memory available dropped by 100 megs, so I guess the trouble was that my grub wasn't listening to my /boot/grub/menu.lst file. I'll look into this; thanks a million for the help!

Cheers,

LeDopore

</div>
</article></li>
 	<li id="comment-1944" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1944" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">LeDopore</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-08-07T21:25:19+00:00"> August 7, 2009 at 9:25 pm </time></div>
</footer>
<div class="comment-content">

&gt;Here's the permanent fix:

I have Ubuntu 9.04 running, but I've upgraded a few times so there might be some funny cobwebs in my installation.

In any case, I found that adding memmap=100M$1800M under # defoptions (see below) propagates the memory reduction when you run update-grub, even though adding it earlier didn't.

## additional options to use with the default boot option, but not with the
## alternatives
## e.g. defoptions=vga=791 resume=/dev/hda5
# defoptions=quiet splash memmap=100M$1800M

Thanks again for the help!

LeDopore

</div>
</article></li>
 	<li id="comment-1958" class="comment even thread-even depth-1"><article id="div-comment-1958" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bogdan C.</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-08-15T14:22:56+00:00"> August 15, 2009 at 2:22 pm </time></div>
</footer>
<div class="comment-content">

&gt;You can map multiple intervals of your ram. Lets say that you want to "ban"

20 MB from 550 to 570
and
35 MB from 800 to 835

You can write at the end of the line mentioned in the post above "memmap=…" multiple times. Like this:

# kopt=root=UUID=a9f39ac2-c67f-418e-93ce-967bdda5e3a4 ro memmap=20M$550M memmap=35M$800M

</div>
</article></li>
 	<li id="comment-1995" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1995" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">prh</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-12-11T12:16:51+00:00"> December 11, 2009 at 12:16 pm </time></div>
</footer>
<div class="comment-content">

&gt;thanks for this really helpful blog –
just a question: when, after adding this (multiple) memmap, I start from grub the windows2000 installation I also have on my hard disk, is then the bad memory also blocked for windows?

prh

</div>
</article></li>
 	<li id="comment-1996" class="comment even thread-even depth-1"><article id="div-comment-1996" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bogdan C.</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-12-12T22:42:54+00:00"> December 12, 2009 at 10:42 pm </time></div>
</footer>
<div class="comment-content">

&gt;Hm… i don't know if this has any effect on Windows.

But you can test this easily: just block, for example, 50 MB of ram. IF Windows shows you (yourAmountOfRam-50) MB then you are one lucky guy 😛

</div>
</article></li>
 	<li id="comment-1998" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1998" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Deftronic</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-12-27T16:30:24+00:00"> December 27, 2009 at 4:30 pm </time></div>
</footer>
<div class="comment-content">

&gt;hello

i'm trying to apply this tutorial but i got ubuntu 9.10 karmic (first installation)
so that i don't got this menu.lst file

i can find a /boot/grub/grub.cfg but there's no kopt=root … in the while content

any help ?

the grub.cfg file content =&gt;
#
# DO NOT EDIT THIS FILE
#
# It is automatically generated by /usr/sbin/grub-mkconfig using templates
# from /etc/grub.d and settings from /etc/default/grub
#

### BEGIN /etc/grub.d/00_header ###
if [ -s /boot/grub/grubenv ]; then
have_grubenv=true
load_env
fi
set default="0"
if [ ${prev_saved_entry} ]; then
saved_entry=${prev_saved_entry}
save_env saved_entry
prev_saved_entry=
save_env prev_saved_entry
fi
insmod ext2
set root=(hd0,6)
search –no-floppy –fs-uuid –set 0fac7a3d-ec9c-4cd2-899e-a78a899c973b
if loadfont /usr/share/grub/unicode.pf2 ; then
set gfxmode=640×480
insmod gfxterm
insmod vbe
if terminal_output gfxterm ; then true ; else
# For backward compatibility with versions of terminal.mod that don't
# understand terminal_output
terminal gfxterm
fi
fi
if [ ${recordfail} = 1 ]; then
set timeout=-1
else
set timeout=10
fi
### END /etc/grub.d/00_header ###

### BEGIN /etc/grub.d/05_debian_theme ###
set menu_color_normal=white/black
set menu_color_highlight=black/white
### END /etc/grub.d/05_debian_theme ###

### BEGIN /etc/grub.d/10_linux ###
menuentry "Ubuntu, Linux 2.6.31-16-generic" {
recordfail=1
if [ -n ${have_grubenv} ]; then save_env recordfail; fi
set quiet=1
insmod ext2
set root=(hd0,1)
search –no-floppy –fs-uuid –set f017e4b3-7a62-48d7-a2fe-d3842503a7e3
linux /vmlinuz-2.6.31-16-generic root=UUID=0fac7a3d-ec9c-4cd2-899e-a78a899c973b ro quiet splash
initrd /initrd.img-2.6.31-16-generic
}
menuentry "Ubuntu, Linux 2.6.31-16-generic (recovery mode)" {
recordfail=1
if [ -n ${have_grubenv} ]; then save_env recordfail; fi
insmod ext2
set root=(hd0,1)
search –no-floppy –fs-uuid –set f017e4b3-7a62-48d7-a2fe-d3842503a7e3
linux /vmlinuz-2.6.31-16-generic root=UUID=0fac7a3d-ec9c-4cd2-899e-a78a899c973b ro single
initrd /initrd.img-2.6.31-16-generic
}
menuentry "Ubuntu, Linux 2.6.31-14-generic" {
recordfail=1
if [ -n ${have_grubenv} ]; then save_env recordfail; fi
set quiet=1
insmod ext2
set root=(hd0,1)
search –no-floppy –fs-uuid –set f017e4b3-7a62-48d7-a2fe-d3842503a7e3
linux /vmlinuz-2.6.31-14-generic root=UUID=0fac7a3d-ec9c-4cd2-899e-a78a899c973b ro quiet splash
initrd /initrd.img-2.6.31-14-generic
}
menuentry "Ubuntu, Linux 2.6.31-14-generic (recovery mode)" {
recordfail=1
if [ -n ${have_grubenv} ]; then save_env recordfail; fi
insmod ext2
set root=(hd0,1)
search –no-floppy –fs-uuid –set f017e4b3-7a62-48d7-a2fe-d3842503a7e3
linux /vmlinuz-2.6.31-14-generic root=UUID=0fac7a3d-ec9c-4cd2-899e-a78a899c973b ro single
initrd /initrd.img-2.6.31-14-generic
}
### END /etc/grub.d/10_linux ###

### BEGIN /etc/grub.d/20_memtest86+ ###
menuentry "Memory test (memtest86+)" {
linux16 /memtest86+.bin
}
menuentry "Memory test (memtest86+, serial console 115200)" {
linux16 /memtest86+.bin console=ttyS0,115200n8
}
### END /etc/grub.d/20_memtest86+ ###

### BEGIN /etc/grub.d/30_os-prober ###
if [ ${timeout} != -1 ]; then
if keystatus; then
if keystatus –shift; then
set timeout=-1
else
set timeout=0
fi
else
if sleep –interruptible 3 ; then
set timeout=0
fi
fi
fi
### END /etc/grub.d/30_os-prober ###

### BEGIN /etc/grub.d/40_custom ###
# This file provides an easy way to add custom menu entries. Simply type the
# menu entries you want to add after this comment. Be careful not to change
# the 'exec tail' line above.
### END /etc/grub.d/40_custom ###

</div>
</article></li>
 	<li id="comment-1999" class="comment even thread-even depth-1"><article id="div-comment-1999" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">jordanwb</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2009-12-31T16:14:25+00:00"> December 31, 2009 at 4:14 pm </time></div>
</footer>
<div class="comment-content">

&gt;Thanks. I got a 1 Gigabyte stick with one bad address and I don't want to throw it out. I'll try this.

</div>
</article></li>
 	<li id="comment-2006" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2006" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bogdan C.</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-01-15T12:14:08+00:00"> January 15, 2010 at 12:14 pm </time></div>
</footer>
<div class="comment-content">

&gt;<b>Deftronic</b> … i had this problem too.

Now (in Ubuntu 9.10) the boot loader is upgraded and the menu.lst file <b>doesn't exist</b>. Stop searching it.

The solution is… different now.

You have to edit the <b>/etc/default/grub</b> file (or <i>grub.cfg</i>; i don't know exactly… i don't have my computer with linux near me). Of course… you have to have admin rights, so edit this file with <b>sudo</b>.

Now… you will se something like this:
<i># If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.</i>

<i> </i><i>GRUB_DEFAULT=0
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=false
GRUB_TIMEOUT="10"
GRUB_DISTRIBUTOR=`lsb_release -i -s 2&gt; /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash <b>memmap=54M\\\$970M</b>"
GRUB_CMDLINE_LINUX=""</i>
<b>save</b> the file and <b>run</b>
<i>sudo update-grub</i>

Those <b>backslashes</b> are important so don't forget them. And 3 of those are now necessary, as you can see in the code posted above.
Of course, you can map, as before, more than one part of your memory. In this case the above line will look something like this:
<i>GRUB_CMDLINE_LINUX_DEFAULT="quiet splash <b>memmap=14M\\\$170M</b> <b>memmap=54M\\\$340M</b> <b>memmap=34M\\\$670M</b>"</i>

Anyway… hope this helped you a little 🙂

</div>
</article><article id="div-comment-4822" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Gimmy</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-09-05T22:27:29+00:00"> September 5, 2014 at 10:27 pm </time></div>
</footer>
<div class="comment-content">

hi all,

Thank you for all the info.
I am experiencing problems with my RAM, so I decided to use Memtest86+ to locate the errors and Memtester to double check them.
I tried then to use Memtester as you suggest here, but I am not sure about the description you gave. I downloaded the last version and from the web I understood that:

sudo memtester 750M

is just going to lock (malloc) a block of 750MB in size and test it without consideration of WHERE this block is.
If the aim is to test a specific area, it is necessary to give memtester a physical address and then the amount of MB from there onwards. e.g.:
for testing 100MB after a certain address 0xfffblabla

sudo memtester -p 0xffffblabla 100M

Does anybody know how I can get the physical address of the defecting memory?
Thanks!

</div>
</article>
<ol class="children">
 	<li id="comment-4827" class="comment byuser comment-author-bryan bypostauthor even depth-2 parent"><article id="div-comment-4827" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-09-06T01:20:50+00:00"> September 6, 2014 at 1:20 am </time></div>
</footer>
<div class="comment-content">

Yup, you understand why memtester isn’t as useful as far as I’ve found. I do believe memtester will allocate contiguous memory though, so if you know where it is from memtest86+ you should be able to get it to allocate to cover the bad spot.

</div>
</article>
<ol class="children">
 	<li id="comment-4837" class="comment odd alt depth-3"><article id="div-comment-4837" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Gimmy</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-09-06T14:49:24+00:00"> September 6, 2014 at 2:49 pm </time></div>
</footer>
<div class="comment-content">

Yes, but I am not sure how to read the memtest86+ output…
The list of errors is not very clear to me.

I tried to isolate some areas using grub.
I’m waiting to see as it goes. fingers crossed.

</div>
</article></li>
</ol>
</li>
</ol>
</li>
 	<li id="comment-2023" class="comment even thread-even depth-1"><article id="div-comment-2023" class="comment-body"><footer class="comment-meta"></footer></article></li>
 	<li id="comment-5690" class="comment even thread-odd thread-alt depth-1 parent"><article id="div-comment-5690" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Myron Shank, M.D., Ph.D.</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-11-22T18:57:56+00:00"> November 22, 2014 at 6:57 pm </time></div>
</footer>
<div class="comment-content">

I had similar problems, but found the available instructions incomplete and confusing, so I wrote these instructions to assume as little background as possible.

1. Run Memtest86+ in “badram” output (preferably from a bootable external medium). When it starts, type
“c” (“configuration”), then
“4” (“Error Report Mode”), then
“3” (“Bad RAM Patterns”), then
“0” (“Continue”).
2. Copy the output that follows any lines beginning with “badram=”.
3. Open a terminal (command line).
4. Change to the directory where the “grub” file is located. For example, “cd /etc/default”.
5. With “root” privileges, use a text editor to open the “grub” file (If you are not signed in as “root,” use “sudo.” You will be asked for the “root” password. In Linux and other Unix derivatives, “root” is equivalent to Windows’ “Administrator.”). For example, type “sudo nano grub” (This temporarily changes the user to “root” and opens the file “grub” with the “nano” text editor.)
6. Find the section describing memtest.
Uncomment the last line (Delete the special character at the beginning of the line, such as “#,” “&gt;,” or “!”). This makes it active (for example, “#GRUB_BADRAM=” becomes “GRUB_BADRAM=”).
Replace the example addresses (following “GRUB_BADRAM=”), with the “badram=” output that you copied from Memtest86+ (for example, “0x98f548a0,0xfffffffc”).
Save (or “write out”) your changes.
7. In the terminal (command line), update the “grub.cfg” file, by typing “sudo update-grub”.
8. Reboot.

</div>
</article>
<ol class="children">
 	<li id="comment-6632" class="comment odd alt depth-2"><article id="div-comment-6632" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">James R</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2015-03-07T06:10:07+00:00"> March 7, 2015 at 6:10 am </time></div>
</footer>
<div class="comment-content">

Myron, I found your comment to be quite instructive, thank you.

</div>
</article></li>
</ol>
</li>
</ul>
 </body></html>