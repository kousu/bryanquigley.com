<html><body><p>Are you running i386 (32-bit) Ubuntu?   We need your help to decide how much longer to build i386 images of Ubuntu Desktop, Server, and all the flavors.

There is a real cost to support i386 and the benefits have fallen as more software goes 64-bit only.

Please fill out the survey here ONLY if you currently run i386 on one of your machines.  64-bit users will NOT be affected by this, even if you run 32-bit applications.
<a href="http://goo.gl/forms/UfAHxIitdWEUPl5K2">http://goo.gl/forms/UfAHxIitdWEUPl5K2</a></p></body></html>