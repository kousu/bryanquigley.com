<html><body><h3>Running 32 bit Ubuntu when the hardware technically can do 64 bit
<a href="/wp-content/uploads/2014/11/32-bit-running-on-64-bit-capable-hardware.png"><img class="alignnone size-full wp-image-2099" src="/wp-content/uploads/2014/11/32-bit-running-on-64-bit-capable-hardware.png" alt="32 bit running on 64 bit capable hardware" width="810" height="497"></a></h3>
<ul>
	<li>hardware issues varied from EUFI 32 bit only, to printer and driver issues</li>
	<li>application included wine (try building wine on 64 bit...)  and virtualization
<ul>
	<li>some 64 bit users use 32 bit images for virtualization to use less RAM</li>
	<li>Not in survey but I know of others who use 32 bit specifically to work with Android.</li>
</ul>
</li>
</ul>
<h2>Arch vs Desktop Environment vs Release</h2>
<a href="/wp-content/uploads/2014/11/Desktop-environments-vs-arch-vs-release.png"><img class="alignnone wp-image-2100" src="/wp-content/uploads/2014/11/Desktop-environments-vs-arch-vs-release.png" alt="Desktop environments vs arch vs release" width="858" height="321"></a>

Please do not use this to really compare desktop environments!  If multiple answers I took the least resource intensive one! (Next time I do this.. I should just require users to pick a primary one)
<h3> Impacts over Releases</h3>
<a href="/wp-content/uploads/2014/11/Impact.png"><img class="alignnone size-full wp-image-2101" src="/wp-content/uploads/2014/11/Impact.png" alt="Impact" width="789" height="416"></a>
<ul>
	<li>Switch from Ubuntu - also includes plans to stay on old unsupported version until hardware dies</li>
	<li>Moderate is somewhat a catch all</li>
	<li>If I do this again, I should just have a 1-5 sliding scale, in addition to a text field.</li>
	<li>Users are concerned about having to throw out old machines, not having an upgrade path to go from 32-&gt; 64 bits, and the cost to upgrade.</li>
	<li>Select Comments (many more in the raw data of course!)</li>
	<li>
<ul>
	<li>As an aspiring software developer, phasing out 32 bit support would be great for me as it means one less build to maintain.</li>
	<li>I plan on reinstalling Ubuntu on this laptop as a 64bit install at some point anyway.</li>
	<li>Unless the schedule changes, no impact. We're planning to do the switch late 2015 / early 2016.</li>
	<li>
</ul>
</li>
	<li>
<ul>
	<li>I will have to stay on 16.04 forever on that machine. The needed drivers are not going to be available in an open-source form.</li>
</ul>
</li>
</ul>
<ul>
	<li>
<ul>
	<li>My parents + my children have no PC</li>
	<li>we have old PC's in the hospital and i don't think this hardware would be upgraded.</li>
	<li>If the majority of freely given computers we receive are still 32-bit by then, we'd have to respin another distro. But, like PowerPC; all good things must come to an end.</li>
	<li>Just need to figure out how to make the switch. If it means re-installing, bah.</li>
	<li>It is terrible, because my eeePC only has 1GB in it.</li>
	<li>One more reason to decommission the hardware.</li>
</ul>
</li>
</ul>
I think the original plan can still work, but like any good survey we know have more questions to ask!
<ul>
	<li>Lubuntu/Xubuntu support for 14.04 LTS is 3 years not 5.   It's going to be a LOT higher impact if they don't have support in 2019/2020 (which would be the case if 16.04 is 3 years too).   This could obviously be mitigated by moving 32 bit to ports and having it be opt in.  Lubuntu/Xubuntu 18.04 with 3 years would get us to 2021.</li>
	<li>What can we do to make virt use less RAM?  (Lots of Virtualbox)</li>
	<li>What can we do to make bare metal use less RAM?</li>
	<li>Building Wine on 64 bit? (The two easiest methods are defunct if we remove 32 bit images I think... </li>
	<li>Can we do an actual upgrade path?  Or at least start officially testing 32-&gt;64 "upgrade" re-installs?</li>
</ul>
Just to complete the application compatibility story (not from survey), Games are starting to be 64-bit only:
<ul>
	<li>Unreal Engine 4 - <a href="https://wiki.unrealengine.com/Linux_Support">https://wiki.unrealengine.com/Linux_Support</a></li>
	<li>X-Com and likely future Feral ports - <a href="http://www.gamingonlinux.com/articles/gamingonlinux-interviews-feral-interactive-about-xcom-linux-game-development.3946">http://www.gamingonlinux.com/articles/gamingonlinux-interviews-feral-interactive-about-xcom-linux-game-development.3946</a></li>
	<li>War Thunder - <a href="http://warthunder.com/en/news/2608-war-thunder-is-now-available-on-linux-en">http://warthunder.com/en/news/2608-war-thunder-is-now-available-on-linux-en</a></li>
</ul>
Raw Data can be found here: <a href="https://docs.google.com/spreadsheets/d/1iA062pCR1ayAMEKveUToEhq--9awyDXTEaL4fhsj8TU/edit?pli=1#gid=0">https://docs.google.com/spreadsheets/d/1iA062pCR1ayAMEKveUToEhq--9awyDXTEaL4fhsj8TU/edit?pli=1#gid=0</a></body></html>
