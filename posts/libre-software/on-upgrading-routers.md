<html><body><p>Below are the speedtests of two different routers using a wired connection.

Actiontec (about 2011) - 53.22 MB (down) 8.23 (up)
Linksys WRT54G v2 (about 2004) - 23 MB (down) 7.76 (up)

Generally I like the older Linksys routers because I can install OpenWRT on them.  With my faster internet connection they can't really stand up to new routers though.  Worth checking if your router is the bottleneck in your Internet experience.

Speaking of new routers, I've been researching to buy with IPv6 support and preferably an open firmware out of the box.  I haven't been able to find one yet...Buffalo is a great choice for the open part, but doesn't have IPv6 by default.  In addition, the DD-WRT project appears to have stalled somewhat this last year.  In an ideal world I'd get a router who's manufacturer officially loads OpenWRT and enables IPv6 out of the box.   Am I missing said vendor?

<strong>Comments</strong>

 
</p><ul>
 	<li id="comment-2337" class="comment even thread-even depth-1"><article id="div-comment-2337" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Claes Comly</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-12-30T21:06:29+00:00"> December 30, 2012 at 9:06 pm </time></div>
</footer>
<div class="comment-content">

Coincidentally im needing to upgrade my linksys router. Wish i could buy an ubuntu router. Did read some about asus routers recently. Looks like where im going.

</div>
</article></li>
 	<li id="comment-2338" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2338" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Nathaniel McCallum</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-12-31T01:50:52+00:00"> December 31, 2012 at 1:50 am </time></div>
</footer>
<div class="comment-content">

<a href="http://wiki.openwrt.org/toh/start" rel="nofollow">http://wiki.openwrt.org/toh/start</a>

I’m a fan of this one:
<a href="http://wiki.openwrt.org/toh/netgear/wndr3800" rel="nofollow">http://wiki.openwrt.org/toh/netgear/wndr3800</a>

</div>
</article></li>
 	<li id="comment-2339" class="comment even thread-even depth-1"><article id="div-comment-2339" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="http://patrakov.blogspot.com/" rel="external nofollow">Alexander E. Patrakov</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-12-31T14:56:13+00:00"> December 31, 2012 at 2:56 pm </time></div>
</footer>
<div class="comment-content">

DD-WRT on MIPS-based routers has been mostly superseded by numerous clones of Tomato USB. I use this mod at home: <a href="http://tomato.groov.pl/" rel="nofollow">http://tomato.groov.pl/</a> . It has IPv6, can correctly translate IPv4 addresses in SIP packets (unlike DD-WRT), and (unlike OpenWRT) can route 100 Mbit/s on ASUS RT-N16.

As for Atheros-based routers, OpenWRT is the remaining alternative.

</div>
</article></li>
 	<li id="comment-2340" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2340" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Gregg</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-12-31T23:22:08+00:00"> December 31, 2012 at 11:22 pm </time></div>
</footer>
<div class="comment-content">

Asus n-66u will do ipv6 and you can put tomato (or dd-wrt?) On as firmware

</div>
</article></li>
</ul>
 </body></html>