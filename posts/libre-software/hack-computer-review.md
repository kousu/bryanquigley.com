<!--
.. title: Hack Computer review
.. slug: hack-computer-review
.. date: 2019-06-17 00:00:00 UTC
.. tags: 
.. category: libre-software
.. link: 
.. description: 
.. type: text
-->

I bought a [hack computer](https://hack-computer.com/) for $299 - it's designed for teaching 8+ year olds programming.  That's not my intended use case, but I wanted to support a Linux pre-installed vendor with my purchase (I bought an OLPC back in the day in the buy-one give-one program).

I only use a laptop for company events, which are usually 2-4 weeks a year. Otherwise, I use my desktop. I would have bought a machine with [Ubuntu pre-installed](https://wiki.ubuntu.com/BuyPCWithUbuntu) if I was looking for more of a daily driver. 

The underlying specs of the [ASUS Laptop E406MA](https://www.asus.com/Laptops/ASUS-Laptop-E406MA/) they sell are:

 * 14.0" (16:9) LED-backlit HD (1920x1080) 60Hz Anti-Glare Panel with 45% NTSC
 * 4GB DDR4 
 * 64G EMMC
 * [Intel® Pentium® Silver N5000 Processor](https://www.intel.com/content/www/us/en/products/processors/pentium/n5000.html)

#Unboxing and first boot
![Unboxing](/images/2019-05-hack-computer/unbox1.jpg)

Included was an:

 * introduction letter to parents
 * tips (more for kids)
 * 2 pages of hack stickers
 * 2 hack pins
 * ASUS manual bits
 * A USB to Ethernet adapter
 * and the laptop:

![Laptop in sleeve](/images/2019-05-hack-computer/unbox2.jpg)
![Laptop out of sleeve](/images/2019-05-hack-computer/unbox3.jpg)
![first open](/images/2019-05-hack-computer/unbox4.jpg)

First boot takes about 20 seconds. And you are then dropped into what I'm pretty sure is GNOME Initial Setup.  They also ask on Wifi connections if they are metered or not.

![first open](/images/2019-05-hack-computer/firstboot.jpg)

There are standard philips head screws on the bottom of the laptop, but it wasn't easy to remove the bottom and I didn't want to push - I've been told there is nothing user replaceable within.

# The BIOS

The options I'd like change are there, and updating the BIOS was easy enough from the BIOS (although no LVFS support..).

![bios ez mode](/images/2019-05-hack-computer/bios-ezmode.jpg)
![bios advanced](/images/2019-05-hack-computer/bios-advanced.jpg)

# A kids take

Keep in mind this review is done by 6 year old, while the laptop is designed for an 8+ year old.

He liked playing the art game and ball game.  The ball game is an intro to the hack content.  The art game is just Krita - see the artwork below.  First load needed some help, but got the hang of the symmetrical tool.

He was able to install an informational program about Football by himself, though he was hoping it was a game to play.
 
![AAAAA](/images/2019-05-hack-computer/aaaaaaaaaaaaaaaaaaaaAAAAAAAAAAAAAAAAAAAAAAAAAAAaa.jpg)
![my favorite](/images/2019-05-hack-computer/f,yjerdddk54ek765o568u4ek66iiii4473uuu3242222yu6u41hu2.jpg)
![water color](/images/2019-05-hack-computer/water_color.jpg)

# Overall

For target market: It's really the perfect first laptop (if you want to buy new) with what I would generally consider the right trade-offs.  Given Endless OS's ability to have great content pre-installed, I may have tried to go for a 128 GB drive.  Endless OS is setup to use zram which will minimize RAM issues as much as possible.  The core paths are designed for kids, but some applications are definitely not.  It will be automatically updating and improving over time.   I can't evaluate the actual Hack content whose first year is free, but after that will be $10 a month.

For people who want a cheap Linux pre-installed laptop: I don't think you can do better than this for $299. 

Pros:

 * CPU really seems to be the best in this price range.  A real Intel quad-core, but is cheap enough to have missed some of the vulernabities that have plaqued Intel (no HT).
 * Battery life is great
 * A 1080p screen

Cons:

 * RAM and disk sizes. Slow eMMC disk.  Not upgradeable.
 * Fingerprint reader doesn't work today (and that's not part of their goal with the machine, it defaults to no password)
 * For free software purists, Trisquel didn't have working wireless or trackpad. The included usb->ethernet worked though.
 * Mouse can lack sensitivty at times
 * Ubuntu: I have had Wifi issues after suspend, but stopping and starting Wifi fixed them
 * Ubuntu: Boot times are slower than Endless
 * Ubuntu: Suspend sometimes loses the ability to play sound (gets stuck on headphones)

I do plan on investiaging the issues above and see if I can fix any of them.

# Using Ubuntu?

My recommendations:

 * Purge rsyslog (may speed up boot time and reduces unnessary writes)
 * For this class of machine, I'd go deb only (remove snaps) and manual updating
 * Install zram-config
 * I'm currently running with Wayland and Chromium
 * If you don't want to use stock Ubuntu, I'd recommend Lubuntu.

# Dive deeper

 * [Benchmarks showing it's slower then my desktop.. mostly a duh.  But can let you compare to your computer.  Tested with Wayland](https://openbenchmarking.org/result/1905204-HV-1905196HV13)
 * [Spectre tool output](/images/2019-05-hack-computer/cpu_vulnerabilities)
 * [lsusb](/images/2019-05-hack-computer/lsusb)	
