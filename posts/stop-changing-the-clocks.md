<!--
.. title: Stop changing the clocks
.. slug: stop-changing-the-clocks
.. date: 2018-02-19 00:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

[Florida](http://floridapolitics.com/archives/256085-everybody-loves-dst-daylight-saving-time-time-wins-committee), Tennessee, the [EU](http://www.bbc.com/news/world-europe-42997518) and [more](http://www.landlinemag.com/story.aspx?storyid=71718)  are considering one timezone for the entire year - no more changing the clocks. [Massachusetts had a group](http://boston.cbslocal.com/2017/11/01/massachusetts-time-zone-change-daylight-saving/) study the issue and recommend making the switch, but only if a majority of Northeast states decide to join them.  I would like to see the NJ legislature vote to join them.

Interaction between countries would be helped by having one less factor that can impact collaboration. Below are two examples of ways this will help.

# Meeting Times
Let's consider a meeting scheduled in EST with partipants from NJ, the EU, and Arizona.  
NJ - normal disruption of changing times, but the clock time for the meeting stays the same.  
Arizona - The clock time for the meeting changes twice a year.  
EU - because they also change their clocks at different points throughout the year.  Due to this, they have 4 clock time changes during each year.

This gets more complicated as we add partipants from more countries. UTC can help, but any location that has a time change has to be considered for both of it's timezones.

# Global shift work or On-call
Generally, these are scheduled in UTC, but the shifts people actually work are in their local time. That can be disruptive in other ways, like finding child care.


*In conclusion*, while these may be minor compared to other concerns (like the potential health effects associated with change the clocks), the concerns of global collaboration should also be considered.
