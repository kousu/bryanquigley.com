<html><body><p>I'm pleased to say that DuckDuckGo is now included as a search engine option in Firefox in the latest Ubuntu 12.10 release.  DuckDuckGo and Twitter are the only search engines that are secure by default via HTTPS.

As a brief comparison I searched for pidgin in the Unity Dash, Google, and DuckDuckGo.  Which results do you like better?

Starting with Google and Unity Dash:

<a href="http://bryanquigley.com/wp-content/uploads/2012/10/GoogleSearch.png"><img class="alignnone size-full wp-image-1470" title="GoogleSearch" src="http://bryanquigley.com/wp-content/uploads/2012/10/GoogleSearch.png" alt="" width="522" height="730"></a><a href="http://bryanquigley.com/wp-content/uploads/2012/10/UnitySearch.png"><img class="alignnone size-full wp-image-1471" title="UnitySearch" src="http://bryanquigley.com/wp-content/uploads/2012/10/UnitySearch.png" alt="" width="642" height="390"></a>

And Now for DuckDuckGo!

<a href="http://bryanquigley.com/wp-content/uploads/2012/10/DuckDuckGoSearch2.png"><img class="alignnone size-full wp-image-1469" title="DuckDuckGoSearch2" src="http://bryanquigley.com/wp-content/uploads/2012/10/DuckDuckGoSearch2.png" alt="" width="998" height="690"></a>

DuckDuckGo separates your search term into different uses, letting you further refine your search results or get instant answers from Wikipedia, dictionaries and many more sources.

DuckDuckGo doesn't track you or bubble you (customize your search so you rarely will be shown another point of view).  They also are <a href="http://duckduckhack.com/">partially open source</a> and made it a goal to give back <a href="https://duck.co/topic/foss-donation-nominations">10% of gross revenue</a> to open source projects.

For a better overview of DuckDuckGo check out their <a href="https://duckduckgo.com/about.html">about</a> page, the introductory video is really nice.   This is just the start of a relationship between DuckDuckGo and Canonical, in fact they haven't even signed a contract yet.  Hopefully the relationship grows and enhances both projects.

<a href="https://duckduckgo.com/">Duck it!</a>

<strong>Comments</strong>

 
</p><ul>
 	<li id="comment-2328" class="comment even thread-even depth-1"><article id="div-comment-2328" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Shawn</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-11-24T09:57:15+00:00"> November 24, 2012 at 9:57 am </time></div>
</footer>
<div class="comment-content">

I can’t believe I was actually searching without all this zero-click info before, this is so unique and helpful from DuckDuckGo. I’m relatively busy, so it saves time on loading multiple pages.

</div>
</article></li>
 	<li id="comment-2332" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2332" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Dandare</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-11-28T04:13:13+00:00"> November 28, 2012 at 4:13 am </time></div>
</footer>
<div class="comment-content">

I like the concept of endless scrolling similar to tumblr. It’s so confy. I use the Duck since 2012 and also found the !bang command is a easy and powerful function. Go! DuckDuck Go! you’re the one 🙂

</div>
</article></li>
</ul>
 </body></html>