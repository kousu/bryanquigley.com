<html><body><a href="http://www.goodguide.com">GoodGuide.com</a> reviews products (started with foods) for their health, environmental, and societal impacts so you as a consumer can make better decisions.  They have recently expanded to cover <a href="http://www.goodguide.com/categories/332304-cell-phones">cell phones</a>.   This gave me an <a href="http://getsatisfaction.com/goodguide/topics/free_open_source_as_a_rating">idea</a>.

Basically my idea is, add to the consideration the level of user's software freedom in the phone itself as well as rate the company on how it is contributing to open source upstreams, etc.  <a href="http://web.archive.org/web/20140227152620/http://www.goodguide.com:80/products/332098-palm-pixi-smartphone">Let's take the Palm Pixi Smartphone as an example</a>,

You can break down how they do the review, by expanding Society.  I would propose adding two sections:
<ul>
	<li><strong>Free and Open Source Upstream Engagement</strong> <em>Under </em> Standard Company Social Performance -&gt; Society -&gt; Community Engagement</li>
	<li><strong>User's Software Freedom </strong><em>Possibly Under </em>Standard Company Social Performance -&gt; Consumers</li>
</ul>
<h2>Sounds good?</h2>
It is, except they need to 'identify a relatively authoritative source working in a domain that has  done detailed evaluations that we can then incorporate into our ratings' [<a href="http://getsatisfaction.com/goodguide/topics/free_open_source_as_a_rating">1</a>].   So my question for the planet and readers is, who could provide this information?

I've been thinking about the <a href="https://www.eff.org/">Electronic Frontier Foundation</a> or <a href="https://www.fsf.org/">Free Software Foundation</a> for <strong>User's Software Freedom</strong>.

And the Linux Foundation (and likely the above as well) for <strong>Free and Open Source Upstream Engagement</strong>.

None of them seem to already have a rating system that would be useful in this way.   I will be sending all of these organizations an email soon, asking them if they would want to take this task on.  But I wanted to get more opinions on this idea and other organizations that I should contact.</body></html>