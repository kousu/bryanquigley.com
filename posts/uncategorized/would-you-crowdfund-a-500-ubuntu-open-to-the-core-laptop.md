<html><body><strong>UPDATE 2 (11/28) - </strong>We're 77% of the way to 1000.  I guesstimate we would have raised at least $300,000 if this we're a live campaign.

<strong>UPDATE -</strong> <em>I've removed the silly US restriction.  I know there are more options in Europe, China, India, etc, but why shouldn't you get access to the "open to the core" laptop! </em>
<em>This would definitely come with at least 3 USB ports (and at least one USB 3.0 port).</em>

Since Jolla had success with <a href="https://www.indiegogo.com/projects/jolla-tablet-world-s-first-crowdsourced-tablet">crowdfunding a tablet</a>, it's a good time to see if we can get some mid-range Ubuntu laptops for sale to consumers in as many places as possible.  I'd like to get some ideas about whether there is enough demand for a very open $500 Ubuntu laptop.
<h2>Would you crowdfund this? (Core Goals)</h2>
<ul>
	<li>15" 1080p Matte Screen</li>
	<li>720p Webcam with microphone</li>
	<li>Spill-resistant and nice to type on keyboard</li>
	<li>Intel i3+ or AMD A6+</li>
	<li>Built-in Intel or AMD graphics with no proprietary firmware</li>
	<li>4 GB Ram</li>
	<li>128 GB SSD (this would be the one component that might have to be proprietary as I'm not aware of another option)</li>
	<li>Ethernet 10/100/1000</li>
	<li>Wireless up to N</li>
	<li>HDMI</li>
	<li>SD card reader</li>
	<li>CoreBoot (No proprietary BIOS)</li>
	<li>Ubuntu 14.04 preloaded of course</li>
	<li>Agreement with manufacturer to continue selling this laptop (or similar one) with Ubuntu preloaded to consumers for at least 3 years.</li>
</ul>
<h2>Stretch Goals? Or should they be core goals?</h2>
Will only be added if they don't push the cost up significantly (or if everyone really wants them) and can be done with 100% open source software/firmware.
<ul>
	<li>Touchscreen</li>
	<li>Convertible to Tablet</li>
	<li>GPS</li>
	<li>FM Tuner (and built-in antenna)</li>
	<li>Digital TV Tuner (and built-in antenna)</li>
	<li>Ruggedized</li>
	<li>Direct sunlight readable screen</li>
	<li>"Frontlight" tech.  (think Amazon PaperWhite)</li>
	<li>Bluetooth</li>
	<li>Backlit keyboard</li>
	<li>USB Power Adapter</li>
</ul>
Take my quick survey if you want to see this happen.  If at least 1000 people say "Yes," I'll approach manufacturers.   The first version might just end up being a Chromebook modified with better specs, but I think that would be fine.

Survey Closed.

</body></html>
