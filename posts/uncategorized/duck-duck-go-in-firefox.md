<html><body><p>If you haven't tried Duck Duck Go, give it a try at <a title="https://duckduckgo.com/" href="https://duckduckgo.com/">duckduckgo.com</a>

Learn more about how it protects your privacy and doesn't track you.. <a title="http://donttrack.us/" href="http://donttrack.us/">donttrack.us</a>

If you like the above, support it being added to Firefox's search box! <a title="http://getsatisfaction.com/mozilla/topics/include_duck_duck_go_as_option_by_default_in_search_box" href="http://getsatisfaction.com/mozilla/topics/include_duck_duck_go_as_option_by_default_in_search_box">http://getsatisfaction.com/mozilla/topics/include_duck_duck_go_as_option_by_default_in_search_box</a></p></body></html>