<!--
.. title: Wrong About Signal
.. slug: wrong-about-signal
.. date: 2020-07-06 20:18:45+00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

A couple years ago I was a part of a discussion about encrypted messaging. 

 * I was in the Signal camp - we needed it to be quick and easy to setup for users to get setup.  Using existing phone numbers makes it easy.
 * Others were in the Matrix camp - we need to start from scratch and make it distributed so no one organization is in control.  We should definitely not tie it to phone numbers.

I was wrong.

Signal has been moving in the direction of adding PINs for some time because they realize the danger of relying on the [phone number system](https://signal.org/blog/signal-pins/). Signal just mandated PINs for everyone as part of that switch. Good for security? I really don't think so.  They did it so you could recover some bits of "profile, settings, and who you’ve blocked". 

# Before PIN
If you lose your phone your profile is lost and all message data is lost too. When you get a new phone and install Signal your contacts are alerted that your Safety Number has changed - and should be re-validated.

{{% chart Pie title='Where profile data lives' %}}
        'Your Devices',   [1]
        {{% /chart %}}


# After PIN
If you lost your phone you can use your PIN to recover some parts of your profile and other information.  I am unsure if Safety Number still needs to be re-validated or not.

Your profile (or it's encryption key) is stored on at least 5 servers, but likely more. It's protected by [secure value recovery](https://signal.org/blog/secure-value-recovery/). 

There are many awesome components of this setup and it's clear that Signal wanted to make this as secure as possible. They wanted to make this a distributed setup so they don't even need to tbe only one hosting it. One of the key components is Intel's SGX which has [several known attacks](https://en.wikipedia.org/wiki/Software_Guard_Extensions#Attacks).  I simply don't see the value in this and it means there is a new avenue of attack.

{{% chart Pie title='Where profile data lives' %}}
        'Your Devices',   [1]
        'Signal servers',   [5]
        {{% /chart %}}

# PIN Reuse
By mandating user chosen PINs, my guess is the great majority of users will reuse the PIN that encrypts their phone.  Why?  PINs are re-used a lot to start, but here is how the PIN deployment went for a lot of Signal users:

1. Get notification of new message 
1. Click it to open Signal
1. Get Mandate to set a PIN before you can read the message!

That's horrible.  That means people are in a rush to set a PIN to continue communicating.  And now that rushed or reused PIN is stored in the cloud.

# Hard to leave
They make it easy to get connections upgraded to secure, but their system to [unregister](https://signal.org/signal/unregister/) when you uninstall has been down for at least a week, likely longer.  Without that, when you uninstall Signal it means:

 * you might be texting someone and they respond back but you never receive the messages because they only go to Signal
 * if someone you know joins Signal their messages will be automatically upgraded to Signal messages which you will never receive

# Conclusion
In summary, Signal got people to hastily create or reuse PINs for minimal disclosed security benefits. There is a possibility that the push for mandatory cloud based PINS despite all of the pushback is that Signal knows of active attacks that these PINs would protect against. It likely would be related to using phone numbers.

I'm trying out the [Riot Matrix client](https://about.riot.im/). I'm not actively encouraging others to join me, but just exploring the communities that exist there. It's already more featureful and supports more platforms than Signal ever did.

Maybe I missed something? Feel free to make a [PR to add comments](https://gitlab.com/BryanQuigley/bryanquigley.com/-/blob/master/posts/wrong-about-signal.md)
