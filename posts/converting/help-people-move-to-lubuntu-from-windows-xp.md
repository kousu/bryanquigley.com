<html><body><p>In preparation for the Cherry Hill Library's XP to Linux Installfest I made a website, presentation and worksheet.

<a href="/wp-content/uploads/2014/04/InstallFestPresentation.odp">InstallFestPresentation</a>
Introduces Ubuntu and Lubuntu for non-technical users and also how today's installfest will work.

<a href="/wp-content/uploads/2014/04/InstallFestWorksheet.odt">InstallFestWorksheet</a>
A place for the user to write down what they use their computer for.   And a place for the installfest helpers to write down what works/doesn't in the LiveCD test.

Items mentioned in the worksheet: The liability release is specific to this Installfest.   As for the survey mentioned, I'm very curious just how many non-pae machines or 32 x86 machines there are still in the wild.

Let me know what you think and feel free to modify/use/whatever.  (If you need a license consider the docs under <a href="http://creativecommons.org/licenses/by/4.0/" rel="license">Creative Commons Attribution 4.0 International License</a>).</p></body></html>