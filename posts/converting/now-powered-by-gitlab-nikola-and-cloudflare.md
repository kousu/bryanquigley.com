<!--
.. title: Now powered by GitLab, Nikola, and Cloudlfare
.. slug: now-powered-by-gitlab-nikola-and-cloudflare
.. date: 2017-11-15 03:00:19 UTC
.. tags: Converting, security
.. category: converting
.. link: 
.. description: 
.. type: text
-->

I just finished moving my website from Wordpress to Nikola (static site generator), GitLab (git and hosting), and CloudFlare (CDN, HTTPS and more).

# Why Nikola
Their attitude in the handbook is "DON'T READ THIS MANUAL. IF YOU NEED TO READ IT I FAILED, JUST USE THE THING." 
That's my kind of software methodology. Don't blame the user, make the system better.

It is also a great [handbook](https://getnikola.com/handbook.html) that has had pretty much every question I've asked. Documentation is still essential, but it's nice if the commands are self explanatory.

It just worked to import my Wordpress site (minus comments which I "inlined" or deleted for various reasons). I did do some manual HTML to markdown conversion for pages I want to edit more.

# Why GitLab
I first tried and had Nikola working with GitHub, but [GitLab](https://about.gitlab.com/) gives me:

 *  Automatic building - I don't have to have a separate branch for output, I just git push my changes (or change on the website) - and GitLab will run a job to create my website. I know this is possible on GitHub, but GitLab just makes it easy.
 *  The option to upload SSL Certs. If I need to drop CloudFlare for some reason, I can have GitLab maintain my website using HTTPS (Which I need to because I'm on the HSTS preload list).
 *  Easier drive by contributions. GitLab lets you sign in with Google, Twitter, GitHub, or BitBucket. I'm thinking for suggesting changes to say a paper (or even this blog post!), that will make for a lower barrier to entry.  (Of course, I'd prefer any OpenID but it's better than requiring a new account)

I absolutely love that they have their [*company* handbook](https://about.gitlab.com/handbook/) maintained in Git and public to the world (with merge request welcome!).

# Why CloudFlare
[CloudFlare](https://www.cloudflare.com)'s free plan rocks. And if I ever need to be able to handle more traffic faster, I can upgrade/downgrade as necessary.

 *  Free (Good) SSL with TLS1.3 Beta too
 *  Free IPv6
 *  Free HTTP2
