<html><body><p>I thought I was being smart.  By not buying through AVADirect I wasn't going to be using an insecure site to purchase my new computer.

<a href="/wp-content/uploads/2017/04/avadirect.png"><img class="alignnone size-full wp-image-2312" src="/wp-content/uploads/2017/04/avadirect.png" alt="" width="1099" height="818"></a>

For the curious I ended purchasing through eBay (A rating) and Newegg (A rating) a new Ryzen (very nice chip!) based machine that I assembled myself.   Computer is working mostly ok, but has some stability issues.   A Bios update comes out on the MSI website promising some stability fixes so I decide to apply it.

The page that links to the download is HTTPS, but the actual download itself is not.
I flash the BIOS and now appear to have a brick.

As part of troubleshooting I find that the MSI website has bad HTTPS security, the worst page being:
<a href="/wp-content/uploads/2017/04/msi.png"><img class="alignnone size-full wp-image-2313" src="/wp-content/uploads/2017/04/msi.png" alt="" width="1178" height="872"></a>

Given the poor security and now wanting a motherboard with a more reliable BIOS  (currently I need to send the board back at my expense for an RMA) I looked at other Micro ATX motherboards starting with a Gigabyte which has even less pages using any HTTPS and the ones that do are even worse:
<a href="/wp-content/uploads/2017/04/gigabyte.png"><img class="alignnone size-full wp-image-2314" src="/wp-content/uploads/2017/04/gigabyte.png" alt="" width="1181" height="923"></a>

Unfortunately a survey of motherboard vendors indicates MSI failing with Fs might put them in second place.   Most just have everything in the clear, including passwords.   ASUS clearly leads the pack, but no one protects the actual firmware/drivers you download from them.
</p><table border="0" cellspacing="0"><colgroup span="5" width="96"></colgroup> <colgroup width="134"></colgroup> <colgroup width="96"></colgroup>
<tbody>
<tr>
<td align="left" height="19"></td>
<td align="left">Main Website</td>
<td align="left">Support Site</td>
<td align="left">RMA Process</td>
<td align="left">Forum</td>
<td align="left">Download Site</td>
<td align="left">Actual Download</td>
</tr>
<tr>
<td align="left" height="19">MSI</td>
<td align="left">F</td>
<td align="left">F</td>
<td align="left">F</td>
<td align="left">F</td>
<td align="left">F</td>
<td align="left">Plain Text</td>
</tr>
<tr>
<td align="left" height="19">AsRock</td>
<td align="left">Plain text</td>
<td align="left">Email</td>
<td align="left">Email</td>
<td align="left">Plain text</td>
<td align="left">Plain Text</td>
<td align="left">Plain Text</td>
</tr>
<tr>
<td align="left" height="19">Gigabyte (login site is F)</td>
<td align="left">Plain text</td>
<td align="left">Plain Text</td>
<td align="left">Plain Text</td>
<td align="left">Plain text</td>
<td align="left">Plain Text</td>
<td align="left">Plain Text</td>
</tr>
<tr>
<td align="left" height="19">EVGA</td>
<td align="left">Plain text default/A-</td>
<td align="left">Plain text</td>
<td align="left">Plain text</td>
<td align="left">A</td>
<td align="left">Plain Text</td>
<td align="left">Plain Text</td>
</tr>
<tr>
<td align="left" height="19">ASUS</td>
<td align="left">A-</td>
<td align="left">A-</td>
<td align="left">B</td>
<td align="left">Plain text default/A</td>
<td align="left">A-</td>
<td align="left">Plain Text</td>
</tr>
<tr>
<td align="left" height="19">BIOSTAR</td>
<td align="left">Plain text</td>
<td align="left">Plain text</td>
<td align="left">Plain text</td>
<td align="left">n/a?</td>
<td align="left">Plain Text</td>
<td align="left">Plain Text</td>
</tr>
</tbody>
</table>
A quick glance indicates that vendors that make full systems use more security (ASUS and MSI being examples of system builders).

We rely on the security of these vendors for most self-built PCs.  We should demand HTTPS by default across the board.   It's 2017 and a BIOS file is 8MB, cost hasn't been a factor for years.</body></html>