<!--
.. title: Firefox Snap is the best way to run Beta Firefox
.. slug: snap-firefox-initial
.. date: 2018-09-05 16:53:49 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

**Update 20019-08-23: I've since moved on from running Beta Firefox and switched from using Snaps for it**

First things first.  I haven't been a huge fan of Snaps (despite working for Canonical) or Flatpaks.  Both I felt initally put convenience over security. I believe both are maturing now, but it again puts the evaluation if a package is secure on users - instead of distros which actually have teams to review items before inclusion..  Anyway, with that said: **On to how I am loving the Firefox Snap**

## Backstory
I'm a long time Beta user of Firefox, but I've been using stable for a while cause it's just easier. I generally preferred getting the tarball from Mozilla directly which has a minor issue.  Long story short, Nautilus doesn't want to be a program launcher of files provided in tarballs. In fact, Nautilus just dropped that support entirely - just at the same time where it was [worked around](https://bugzilla.mozilla.org/show_bug.cgi?id=1079662) in Firefox. 

# Snap Install firefox beta
**sudo snap install firefox --beta**

That's it. No unpacking a tarball - no making a desktop entry so you can launch it more easily.  Of course, just leave off --beta if you want to get the stable version of Firefox via a snap.

It uses a separate profile from the deb installed Firefox, but you can only run one at a time. (This is the same if you download a tarball)

Some other steps that will vary based on wha you are doing:
1. Sync your data across (I use mozilla sync) and setup your extensions, etc.
2. Replace the deb based one from your dock and add the snap based one there. (Right click details in Gnome to see which is which)

# Upgrade to 63 beta

So snaps autoupgrade and I soon found myself on Firefox 63 beta and everything looked good until I tried to join a hangout. It didn't work. Since I was using snaps I just tried a:
**sudo snap revert firefox** and I was back on 62 beta in time for my hangout starting in 2 minutes.  (Note: it was likely worth just switching to stable instead with **sudo snap refresh firefox --stable**)

Snaps will automatically upgrade to the next version with the hope that it will have fixed whatever bug made you revert. A few days they released a new version of 63 beta and Hangouts broke again. In this case I determined it was actually a [Google Hangouts/Meet bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1487617).  One good way to tell when different channels have had releases is to look at [https://snapcraft.io/firefox](https://snapcraft.io/firefox).

# Privacy

The snap version does provide more protection from a compromised Firefox.  It also separates the Downloads folder for the snap to a snap specific one at ~/snap/firefox/common/Downloads.

![Access Denied trying to open .ssh from snap](/images/snap-firefox-initial-PrivateFileProtection.png)

# Other issues
* Checkboxes didn't show up for a bit on version 62.
* Multi-account container's has a [bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1489876) in Firefox 63.

From what I can tell these issues are all Firefox beta issues, not issues specific with the snap version.  The only snap specific issue I've ran into is the first time you start a snap it takes a while.

# Conclusion
I'm staying on a Firefox snap. It's faster to change between stable and beta channels. It also seems like it gets updates faster than Firefox in the Ubuntu archive. 

Give it a try today with (or remove the beta for stable): **sudo snap install firefox --beta**
