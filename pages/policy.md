This page cover's my public policy positions and other political matters.

### Voting

I also have a [proposal for how we could evolve our elections in the US](/pages/papers/voting-proposal.html).

Having said that, your local town is where you have the most power to make positive change.

### Better Transportation

How I would prioritize transportation effort:

1.  Walking
2.  Biking and other forms of self-propelled vehicles. (Skateboarding, Scooters, etc)
3.  Mass transportation
4.  Personal motor vehicles (cars/motorcycles/etc)
5.  Commercial vehicles

Walking is everyone's right that government should not be allowed to take away.  Unfortunately due to the creation of roads with no to little consideration for walkers, we are no longer safe walking on many roadways. Generally I view this list as 1 (right) -> 5 (privilege).  The fact that cars are practically required in some neighborhoods needs to be a thing of the past. Higher gas taxes should be instituted for NJ, we currently have [one of the cheapest gas taxes](https://en.wikipedia.org/wiki/Fuel_taxes_in_the_United_States#State_taxes) in the nation.  I would suggest raising it be 5 cents, which would still put us about 4 cents under Delaware.  The primary reasons are to raise revenue and discourage consumption. [Transportation for America    http://t4america.org/](http://t4america.org/)  

###  End the Corn Subsidy

[End the Corn Subsidy   https://endcornsubsidy.wordpress.com/](http://endcornsubsidy.wordpress.com/) http://www.whitehouse.gov/blog/2011/09/19/someday-now-direct-farm-payments-and-president-s-plan-economic-growth-and-deficit-re

### Legalize Marijuana

[http://www.huffingtonpost.com/<wbr>greg-campbell/marijuana-myths_<wbr>b_1434718.html#s882048&title=<wbr>Marijuana_can_cure](http://www.huffingtonpost.com/greg-campbell/marijuana-myths_b_1434718.html#s882048&title=Marijuana_can_cure)

### Open Source Benefits/Taxes for States

Could be based off of: https://github.com/CMSgov/BenefitAssist https://gcn.com/articles/2016/10/18/benefit-assist.aspx

### Outdoor Lightning

[http://www.darksky.org](http://www.darksky.org) [http://news.discovery.com/animals/bugs-streetlights-120522.html](http://news.discovery.com/animals/bugs-streetlights-120522.html) [http://cs.astronomy.com/asy/b/astronomy/archive/2015/07/29/humans-cling-to-their-primal-fear-of-the-dark.aspx](http://cs.astronomy.com/asy/b/astronomy/archive/2015/07/29/humans-cling-to-their-primal-fear-of-the-dark.aspx)
