<!--
.. title: Voting Proposal 
.. slug: voting-proposal
.. date: 2018-10-21
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

This is a rough work in progress, please feel free to contribute on [Gitlab.com](https://gitlab.com/BryanQuigley/bryanquigley.com/tree/master/pages/papers).

#TODO
 * hash out a clear introduction 
 * Do an example election with the sample setups

# Problem Statement
Many from the recent election have focused their attention on the disagreement between the electoral college and the popular vote.  I don't disagree that this is a problem, but it's hardly the worst problem with how we choose the President of the USA.  If we are to put in the effort to reform our presidental election process we should look deeper then just the most recent problem.

 * Our two main parties nominated the two least popular candidates in US history and third parties still did not fair well at all.
 * When first envisioned the VP was the runner up in the general election.  This meant there was an actual threat to impeachment and removal.  In the current system the VP is more likely to just pardon the president and take office.  We desperately need more ability to keep our presidents accountable.
 * The electoral college itself is actually useless.  There is no purpose in actually having electors, if we keep that process we might as well just let state law make the decision clear.  (AKA PA says all their votes go the winner and it does, no other people involved)
 * Over (10 Million) American Citizens are not allowed to vote for president because of where they live.  In many cases, they are being taxed without representation. 
 * The current sysem promotes voter suppression and not voter turnout.  Why?  You get votes based on elgible individuals, NOT from actual voters. The states that get encourage more people to vote *should* get an advantage.

Some actual advantages:
It's 50 seperately run elections which makes it more difficult to directly hack and easier to find anomalies in specific state vote records.

Some though to be advantages:
It gives small states power.  I believe that's a myth.  It gives small states power when they *happen* to be competive **or** have rules that make them more so, like New Hampshire.

# Proposal
Each state has a choice on it's own voting system with the requirement only existing in the form the ballets must be submitted in.  Every ballet most be submitted in full with:
Each candidates positive score normalized to 1

Furthermore each state must submit a specific model for scoring their ballot specifically including:

 * What method they want a negative score to affect the result. 
 * If they allow alliances (giving your vote from one person to another) between ballots and for what kinds of ballots.
 * A period after voting, party alliances must be submitted as well.

# Sample Setups?
 * The simplest state. Approval Voting *
Allows just options 0 (no), 1 (yes) for each candidate.
No negative scoring and no alliances.


 * Next up, a basic [range/scoring](https://en.wikipedia.org/wiki/Range_voting) state. *
Allows just scoring options 0 (no), 1, 2 for each candidate.
No negative scoring and no alliances.


 * Now a more complicated scoring state. and alliances *
Allows scoring options 0, 1, 2 for each candidate.
Allow alliances between any positive candidates. 


Other references for the future
http://www.philly.com/philly/opinion/20161219_Electoral_College_worth_a_little_more_study.html


