<!--
.. title: 2020 presidential websites tracker
.. slug: 2020-presidential-websites-tracker
.. date: 2019-02-23
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Results generated from 
 * [Hardenize](https://www.hardenize.com)
 * [Google Pagespeed Insights](https://developers.google.com/speed/pagespeed/insights/)

No one had MTA-STS, TLS-RPT, or DANE. No one had Nameservers misconfigured...  and assuming no one had HTTP wrong too.. no one had Certs, Cookies, or Mixed Content issues.

| Worth | What                            | Description                                                                                                   |
| --    | ----------------                | -----------------------------------------------------------------------------------------------------------   |
| --    | ✓                                             | Good rating          |
| --    | ✗                                             | Not present         |
| --    | ⚠                                             | Possibly misconfigured or partially setup |
| --    | GPI                             | Google Page speed insights on domain- mobile, desktop. Field data - Average=A=M, S=Slow, I=Insufficient data. Numbers vary by a bit on each run |
| 5     | Having both Ipv4 and IPv6       |  aside from a push for a more modern internet, may have an impact on performance on cell phone networks       |
| 10    | DNS - DNSSEC, DNS CAA           | possible indicators for internal security                                                                     |
| 20    | Email - SPF (5/2), DMARC (15/5) | Email security was a huge part of 2020 election, partial credit possible                                      |
| 20    | STS (10/5) +  Preloaded (10)    |  may help protect campaign staff and vollunteers (5 if partial), preloaded helps all visitors/performance     |
| 6     | MSF                             |  Modern Security Features - Content Security Policy, Subresource Integrity, Expected CT                          |
| 6     | AS                              |  Application Security - Frame Options, XSS Protection, Content Type Options                                      |
| 8     | Has TLS1.3                      |  better security, better performance                                                                          |
| 10    | GPI - Audit Results             |  Mobile GPI Score divied by 5                                                                           | 
| 10    | GPI - Real Results              |  Mobile GPI Slow or Insuffient data=0, Slow = 2, Average=5                                                            |
| 100   | -----                           |  Maximum score                                                                                                 |


# Current - 2020-01-20

| Candidate              | My | IP|DNS |Email| TLS1.| STS | MSF  | AS      | Host        |  GPI    | Notes |
| ---------------------- | -- | - |--- | --- | --   | --  | ---- | -----   | ----------  |  -----  | ----- |
| joebiden.com           | 62 | ✓ |✗✗ | ✓✓ | 2    | ✓✓ | ✗✗✗ | ✗✗✗ |  pantheon.io| 53M,95M | Mobile 77M -> 53M   | 
| amyklobuchar.com       | 59 | ✓ |✗✗ | ✓✓  | 0-3 | ✗✗ | ✗✗✗ | ✓✓✓ | Cloudflare  | 42M,90M | Mobile 62M->42M  |
| tulsi2020.com          | 57 | 0 |✗✗ | ✓✓  | 0-3 | ✓✗ | ✗✗✓ | ✓✗✓ | Cloudflare | 30M,78M | Switched from TLS1.2 to 0-3. Lost IPv6, switched to cloudflare |
| peteforamerica.com     | 57 | ✓ |✓✗ | ✓✓  | 0-3 | ✓✗ | ✗✗✓ | ✓✓✓ | Cloudflare  | 15S,73M | Mobile 20S -> 15S, Added DNSSEC, Fixed DMARC |
| elizabethwarren.com    | 40 | 0 |✓✗ | ✓✓  | 0-3 | ⚠✗ | ✗✗✓ | ✓✗✗ | Cloudflare  | 5S,51S | Mobile 7M -> 5S, Insecure Cookie (-5) |
| berniesanders.com      | 40 | ✓ |✗✗ | ✓*  | 0-3 | ⚠✗ | ✗✗✓ | ✓✗✓ | Cloudflare  | 55M,95M | Mobile 44M-55M |
| tomsteyer.com          | 40 | ✓ |✗✗ | ✓✓  | 2   | ⚠✗ | ✗✗✗ | ✗✗✗ |             | 26M,66M | Mobile 15S->26M, Better email security | 
| yang2020.com           | 35 | ✓ |✗✗ | ✓✓  | 2   | ✗✗ | ✗✗✗ | ✗✗✗ | pantheon.io | 12M,62M | Mobile 12M->40M, still 5 minute STS  |

# Before Debates

Wasn't recording partially setup DMARC before - noted with *.

| Candidate              | My | IP|DNS |Email| TLS1.| STS | MSF  | AS    | Host        |  GPI    | Notes |
| ---------------------- | -- | - |--- | --- | --   | --  | ---- | ----- | ----------  |  -----  | ----- |
| joebiden.com           | 65 | ✓ |✗✗ | ✓✓| 2    | ✓✓ | ✗✗✗ | ✗✗✗ |  pantheon.io| 77A,98A | Fixed misconfig, now on preload list, and new full DMARC email setup! has noticeable activiey in GPI |
| corybooker.com         | 56 | ✓ |✗✗ | ✓✓  | 0-3 | ⚠✗ | ✗✓✓ | ✗✗✓ | Cloudflare  | 61A,95A | Improved real and audit GPI! redirect issue remains, new Mixed Content Warning (-5)! Enables full email security! |
| hickenlooper.com       | 55 | ✓ |✗✗ | ✓✓ | 1-3 | ✓✗ | ✗✗✓ | ✓✓✓ | Cloudflare   | 22I,67I | Added HSTS , full email security,  full AS,  and Expected CT. Changed TLS generally better, slight website improvement, switched to cloudflare |
| amyklobuchar.com       | 49 | ✓ |✗✗ | ✓*  | 0-3 | ✗✗ | ✗✗✗ | ✓✓✓ | Cloudflare  | 75A,98A | DMARC policy not full working, added tls1.0 (ok) |
| elizabethwarren.com    | 48 | 0 |✓✗ | ✓✓  | 0-3 | ✓✗ | ✗✗✓ | ✗✗✗ | Cloudflare  | 15S,70S | adding DNSSEC and HSTS - but not ready for preloading yet.  website performance got worse |
| michaelbennet.com      | 43 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✓✓✓ | Cloudflare  | 69I,90I | no signifant changes |
| johndelaney.com        | 43 | ✓ |✗✗ | ⚠*  | 2   | ?✗ | ✗✗✗ | ✗✗✗ | pantheon.io  | 26I,73A | not enough mobile visitors for real rating, improved performance for real desktop users.  SPF policy has issues.  DMARC policy not full working - ?HSTS Policy partially setup?	|
| betoorourke.com        | 40 | ✓ |✓✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✗✗✗ | Cloudflare  | 62A,96A | added DNSSEC, slight speed improvement with real visitors |
| jayinslee.com          | 39 | ✓ |✗✗ | ✓*  | 0-2 | ✓✗ | ✗✗✗ | ✓✗✗ | Cloudfront  | 56I,95I | Added Frame Options and HSTS  |
| marianne2020.com       | 39 | ✓ |✗✗ | ✓*  | 0-3 | ✗✗ | ✓✓✓ | ✓✗✗ | unknown     | 29S,81S | added HSTS, DMARC policy not full working, added other security features |
| berniesanders.com      | 36 | ✓ |✗✗ | ⚠✗  | 0-3 | ⚠✗ | ✗✗✓ | ✗✗✓ | Cloudflare  | 61A,94A | no signifant changes |
| stevebullock.com       | 36 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✓✓✓ | Cloudflare  | 59I,90I | no signifant changes |
| donaldjtrump.com       | 32 | ✓ |✗✗ | ⚠✗  | 0-2 | ✗✗ | ✗✗✗ | ✓✗✗ | Cloudflare  | 90A,100A | no signifant changes | 
| ericswalwell.com       | 32 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✓✓✓ | unknown     | 38I,85I  | no signifant changes |
| weld2020.org           | 31 | 0 |✗✗ | ✓✗  | 2-3 | ✓✗ | ✗✗✗ | ⚠✗✓ | unknown     | 21S,65S | GPI no longer crashes.. may have TLS mail server issues, added HSTS setup.. Other HTTTPS issues though, bunch of other feature changes |
| julianforthefuture.com | 30 | ✓ |✗✗ | ⚠*  | 2  | ✗✗ | ✗✗✗ | ✗✗✗ | pantheon.io  | 88I,99A | DMARC policy not full working, improvement in optimzation, no longer enough mobile visitors  |
| tulsigabbard.org       | 28 | ✓ |✗✗ | ✗✗  | 2  | ✗✗ | ✗✗✗ | ✓✗✓ | pantheon.io  | 65I,97I | no longer enough visitors for GPI , still no mail servers, reversal of what AS enabled..with no email enabled, adjusting to out of 80 - would be 22 |
| kirstengillibrand.com  | 26 | ✓ |✗✗ | ⚠*  | 1-2 | ✓⚠ | ✗✗✓ | ✗✗✗ | unknown     | 61I,91A | no longer enough mobile visitors , DMARC policy not full working, didn't fix redirection issues, preload flag STILL let's anyone do it, open to attack (NO HSTS Points) |
| kamalaharris.org       | 26 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✗✗✗ | Cloudflare  | 32S,65A | Real world performance getting worse for mobile, no other changes|
| billdeblasio.com       | 26 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✗ | ✗✗✗ | Cloudflare  | 40I,90I | Misconfigured basic HTTPS.. redirects back to HTTP then back to HTTPS (-10)  |
| peteforamerica.com     | 25 | ✓ |✗✗ | ✓✗  | 1-2 | ✗✗ | ✗✗✗ | ✗✗✗ | Cloudflare  | 51A,92A | paying more for cloudflare, but way less optimized site, lost ALL AS, lost tls1.0 (good) and tls1.3 (WHY!)  |
| wayneforamerica.com    | 23 | ✓ |✗✗ | ✓✗  | 0-2 | ✗✗ | ✗✓✗ | ✓✗✗ | unknown     | 45I,83I | improvement in performance, but still not enough real visitors.  added "Subresource integrity by removing resources... |
| timryanforamerica.com  | 20 | ✓ |✗✗ | ✓✗  | 0-3 | ✗✗ | ✗✗✓ | ✗✗✗ | unknown     | 5I,24I  | Added expected CT  |
| sethmoulton.com        | 18 | ✓ |✗✗ | ✗✗  | 1-2 | ✗✗ | ✗✗✗ | ✓✗✗ | Cloudflare  | 43S,92S |  got enough visotors for GPI, but website performance is worse, not other changes |
| yang2020.com           | 9  | ✓ |✗✗ | ✗✗  | 2   | ✗✗ | ✗✗✗ | ✗✗✗ | pantheon.io | 12S,31A |  Dropped TLS1.0/1.1 (good) - GPI audit performance hit  |



<br/><br/>

# Original including launches before May 15th, with older grading

| Key   | Means               |
| --    | ----------------    |
| My    | My opinionated overall rating | 
| F     | less than 50 on GPI Mobile. if only have IPs for v4 or v6 in this category  |
| D     | one or less interesting security feature fully enabled   |
| C     | either GPI >90 or 2 or more security features enabled    |
| SSL   | SSLLabs rating |


| Candidate              | My| IP  |DNS | Email | TLS     | HSTS | MSF  | AS    | SSL | Host           |  GPI       | Notes |
| ---------------------- | --| --  |--- | ----  | --      |  --  | ---- | ----- | --- | ------------   |  --------  | ----- |
| peteforamerica.com     | C | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗  | ✗✗✗ | ✓✓✓ | A  | Cloudflare      |  93A,100   | ----- |
| donaldjtrump.com       | C | 2,2 |✗✗ | ⚠✗  | 1.0-1.2 | ✗✗  | ✗✗✗ | ✓✗✗ | A  | Cloudflare      |  94A,100A  | why use cloudflare and not enable TLS1.3? | 
| joebiden.com           | C | 1,2 |✗✗ | ✓✗  | 1.2     | ✓*  | ✗✗✗ | ✗✗✗ | A+ | pantheon.io     |  86I,98I   | *pending to get on preload list, small misconfig on plaintext |
| amyklobuchar.com       | C | 2,2 |✗✗ | ✓✗  | 1.1-1.3 | ✗✗  | ✗✗✗ | ✓✓✓ | A  | Cloudflare      |  73A,96A   | ----- |
| michaelbennet.com      | C | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✓✓✓ | A  | Cloudflare       |  73I,95I   |  |
| berniesanders.com      | C | 2,2 |✗✗ | ⚠✗  | 1.0-1.3 | ⚠✗ | ✗✗✓ | ✗✗✓ | A+ | Cloudflare       |  60A,94    | ----- |
| stevebullock.com       | C | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✓✓✓ | A  | Cloudflare       | 60I,94I   | ----- |
| jayinslee.com          | D | 4,0 |✗✗ | ✓✗ | 1.0-1.2  | ✗✗ | ✗✗✗ | ✗✗✗ | A  | Cloudfront       |  73I,96    |   |
| julianforthefuture.com | D | 1,2 |✗✗ | ⚠✗  | 1.2     | ✗✗ | ✗✗✗ | ✗✗✗ | A  | pantheon.io      |  70S,92A   | ----- |
| kirstengillibrand.com  | D | 5,5 |✗✗ | ⚠✗  | 1.1-1.2 | ✓⚠ | ✗✗✓ | ✗✗✗ | A+ | unknown          | 68S,91A   | not proper redirection according to hardenize..  preload flag let's anyone do it |
| tulsigabbard.org       | D | 1,2 |✗✗ | ✗✗  | 1.2     | ✗✗ | ✗✗✗ | ✗✓✗ | A  | pantheon.io      |  63A,95A   | no mail servers |
| sethmoulton.com        | D | 5,5 |✗✗ | ✗✗  | 1.1-1.2 | ✗✗ | ✗✗✗ | ✓✗✗ | A  | Cloudflare       |  63I,96I  | Cert from GoDaddy |
| betoorourke.com        | D | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✗✗✗ | A  | Cloudflare       |  60S,94S   | ---- |
| corybooker.com         | F | 5,5 |✗✗ | ⚠✗  | 1.0-1.3 | ⚠✗ | ✗✓✓ | ✗✗✓ | A+ | Cloudflare       |  47S,90    | HTTPS redirect misconfiguration reported |
| wayneforamerica.com    | F | 1,0 |✗✗ | ✓✗  | 1.0-1.2 | ✗✗ | ✗✗✗ | ✓✗✗ | A  | unknown          |  32I,721I   | One of his other sites redirects with HTTPS error to this one |
| ericswalwell.com       | F | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✓✓✓ | A  | unknown          |  31I,87I   |  |
| johndelaney.com        | F | 1,2 |✗✗ | ✓✗  | 1.2     | ✗✗ | ✗✗✗ | ✗✗✗ | A  | pantheon.io      |  26S,58S   | ----- |
| kamalaharris.org       | F | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✗✗✗ | A  | Cloudflare       |  28A,65A   | Page score looks worse then it is - youtube |
| elizabethwarren.com    | F | 2,0 |✗✗ | ✓✓  | 1.0-1.3 | ✗✗ | ✗✗✓ | ✗✗✗ | A  | Cloudflare       |  24S,86S   | ----- |
| marianne2020.com       | F | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✓✓✗ | ✓✗✗ | A  | unknown          |  24S,75S    | ----- |
| weld2020.org           | F | 1,0 |✗✗ | ✓✗  | 1.2-1.3 | ⚠✗ | ✓✗✗ | ✓✓✓ | A+ | unknown          |  15S,Crashes | may have TLS mail server issues
| yang2020.com           | F | 1,2 |✗✗ | ✗✗  | 1.0-1.2 | ✗✗ | ✗✗✗ | ✗✗✗ | A  | pantheon.io      |  12S,49A   | ----- |
| hickenlooper.com       | F | 1,0 |✗✗ | ✓✗  | 1.2     | ✗✗ | ✗✗✗ | ✗✗✗ | A  | unknown          |  11I,64I   | ---- |
| timryanforamerica.com  | F | 2,2 |✗✗ | ✓✗  | 1.0-1.3 | ✗✗ | ✗✗✗ | ✗✗✗ | A  | unknown          |  5I,24I   |  |
