<!--
.. title: Ubuntu Monthly Update Cadence
.. slug: ubuntu-monthly-update-cadence
.. date: 2017-12-27 07:30:48 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Feedback/[improvements welcome](https://gitlab.com/BryanQuigley/bryanquigley.com/blob/master/pages/papers/ubuntu-monthly-update-cadence.md).

## Goals 
 * Get fresh software to users faster, faster upstream feedback
 * Be easier for users who use the non-LTS releases to stay updated (I’ve found that many users forget about the need to do major updates)
 * Stager updates so that the release is
   * predictable enough that it’s useful for more users than our current non-LTS releases
   * useful for more business cases (specifically cloud where this could be part of companies CI/General Release process - and therefore remove the need for big release flag days, See Aside: Hypothesis)
   * trivial to triage problems, only have 1 likely culprit changing at once


## Policy
Updates to core components would need to be staggered so that no closely coupled components would see a major release in the same month. For example, these could be the highly coupled components:  
Kernel - Mesa  
Kernel - Systemd  
Mesa - Gnome  
Systemd - Gnome  

The core component update would release at the same time every month (specific day, or 3rd tuesday) and the above is just a sample.

Each core component update would be made available via the Monthly Beta PPA before the update - even if this means it's a beta/RC release of the software. Then it can graduate to -proposed and the usual update cycle.

We'd want to publish a monthly newsletter on what was just released, and projections/possibilities for the next 2 months. 

Whoopsie would be enabled on all Monthly installs by default so that we get more data on crashes from servers, etc.  This would also help us refine our list of highly coupled components.  

## Example Usage
An example of what would have been released in the last year given the above coupling.

2017-01 - Misc other updates. Could be great time for a general debian sync of packages  
2017-02 - Kernel or Mesa available.  Can only pick one.  Let’s go with *kernel*.  
2017-03 - Mesa (from last month), systemd, and Gnome available.  Release *Mesa* and *systemd* together as they aren’t coupled.  
2017-04 - Only *Gnome* (from last month) so release it.  
2017-05 - Kernel and Mesa available.  Release *kernel*.  
2017-06 - Only *Mesa* (from last month) so release it.  
2017-07 - Systemd and kernel available.  Release *kernel*.  
2017-08 - *Systemd* (from last month) and *Mesa*, release them both.  
2017-09 - *Kernel* and *Gnome* available.  Release them both as they aren’t coupled  
2017-10 - Release *Systemd*  
2017-11 - Release *Mesa*  

This is based on these projects having the following release days.
#### Gnome
2017-03 - 3.24  
2017-09-13 - 3.26  

#### Kernel 
2017-09-03 - 4.13  
2017-07-02 - 4.12  
2017-05-01 - 4.11  
2017-02-19 - 4.10  

#### Mesa
2017-02-13 - 17.0  
2017-05-10 - 17.1  
2017-08-late - 17.2  
2017-11-mid - 17.3  

#### Systemd
2017-03-11 - V233  
2017-07-12 - V234  
2017-10-05 - V235  

### Other updates
SRUs can continue as they usually do, except that they would need to be evaluated against whatever major updates are happening at that time.  Ideally/eventually less SRUs will be needed as we will get software from upstream faster. 
 	
We would still need a policy similar to the current microreleases policy for items that aren’t tightly coupled to anything else and have great test cases allowing them to be released at any time.

## Aside: Version Numbers
This is a unique opportunity to simplify our overall version numbering. I suggest having Ubuntu Monthly to have clearly different version numbers.

For the monthly release my prefence is, [a date with dashes](https://xkcd.com/1179/):  
2017-12-26 Monthly (where the number is based on when the last package installed was released)

For LTSes we can simplify them (as many people do incorrectly today):  
18.04.0 LTS= 18.0 LTS  
18.04.1 LTS = 18.1 LTS  

## Aside: What about LTS releases?
They would still happen every two years in April. Nothing about the release cycle of the LTS should change.  What would need deciding is when we split off the LTS release from the Monthly release.

The two options I see are  
 * have a stabilization period before .0 - It doesn't make sense to do much more than 3 months IMHO.
 * or just release the monthly version in March or April and that becomes the .0

## Aside: Hypothesis
More users/businesses would use our non-LTS releases with smaller upgrades every month vs our current upgrade every 6 months requirement. This also matches development methodologies that are more agile.

For example a CI setup of a group targeting Ubuntu Monthly would have their CI setup automatically test on the current version, -proposed, and the Monthly Beta PPA updates.

We can compare the uptake in the release that would be 18.10 to historical non-LTS releases, to see if there is some truth to this. I don't expect businesses to move unless we make more of a committment though, but especially desktop users might be more willing to give it a try.  
