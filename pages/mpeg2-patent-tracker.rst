**Note: I am far from a patent lawyer...**

.. _Posting: https://www.mpegla.com/programs/mpeg-2/patent-list/
.. _Diving_deeper: http://www.myipo.gov.my/en/search-patent/
.. _#US5565923A: https://patents.google.com/patent/US5565923A/en
.. _post: https://yro.slashdot.org/story/18/02/14/1621259/mpeg-2-patents-have-expired
.. _not: https://github.com/raspberrypi/firmware/issues/939


As of their January 1, 2020 Posting_, these are the current patents the MPEGLA patent pool believe's are valid.

 * MY 128994 (possible expiration of 30 Mar 2022)
 * MY 141626-A (possible expiration of 31 May 2025)
 * MY-163465-A (possible expiration of 15 Sep 2032)

I've tried sending complaints to the Malaysia Patent office, but don't believe anything will happen unless someone sues in court.

Does this still impact anyone?  Yes for instance Rapsberry Pis before 4 are still not_ going to try let MPEG-2 be free until all countries are there. (MPEG-2 hardware support has been removed from Pi 4s)


===========
MY-163465-A
===========

:Title: SYNCHRONIZATION ARRANGEMENT FOR A COMPRESSED VIDEO SIGNAL
:Client Reference: 13876MY4/PM/ANA/HJJ
:Received Date: 04 Nov 2015
:Application Number: PI 2015002705
:Grant Date: 15 Sep 2017
:OPI Date: 13 Nov 1994
:Filing Date: 24 Apr 1994
:Expiration Date: 15 Sep 2032
:Renewal Due Date:   Please be aware to renew your patent before this date is reached, or else your patent will become expired. 14 Sep 2020
:Agent: PATRICK MIRANDAH
:Applicant / Owner: GE TECHNOLOGY DEVELOPMENT, INC.,	 	ONE INDEPENDENCE WAY (US)
:Inventors:  	JOEL WALTER ZDEPSKI

:International Patent Classification: C09C 1/00	Treatment of specific inorganic materials other than fibrous fillers Preparation of carbon black	2006.01	11 Dec 2015
     H03M 7/30	. Compression Expansion Suppression of unnecessary data, e.g. redundancy reduction	2006.01	11 Dec 2015

:Priority: UNITED STATES OF AMERICA (US) 	13 May 1993	060,924
:Abstract: Apparatus for developing synchronization of an intermediate layer of signal such as the transport or multiplex layer of a multi-layered compressed video signal, includes at the encoding end of the system apparatus (13,23,25) for including a time stamp reference, such as a count value from a modulo K counter (23), and provision for a differential time stamp related to time stamps of a further compressed signal or differential transit times of respective transport packets. Flags are included in transport packets of signal to indicate the presence or absence of the time stamps or differential time stamps. At the receiving end of the system, circuitry examines respective packets for the condition of the flags to locate specific information such as the time stamps and differential time stamps. A counter (27) is responsive to a controlled receiver clock signal and the count value of this counter is sampled at the arrival of transport packets including a flag indicating the presence of a time stamp. Circuitry is described for using the time stamps and the sampled count values of the receiver counter (36) to provide a signal for controlling the frequency of the receiver clock signal.

:Number of Claims: 16
:Number of pages of claims:  16
:Number of pages of drawing: 8
:Number of pages of full specification: 24

===========
MY-141626-A
===========
:Title: SYNCHRONIZATION ARRANGEMENT FOR A COMPRESSED VIDEO SIGNAL
:Client Reference: RP-010050
:Received Date: 18 Oct 2006
:Application Number: PI 20064345
:Grant Date: 31 May 2010
:OPI Date: 13 Nov 1994
:Filing Date: 27 Apr 1994
:Expiration Date: 31 May 2025
:Renewal Due Date:   Please be aware to renew your patent before this date is reached, or else your patent will become expired. 30 May 2020
:Agent: PATRICK MIRANDAH
:Applicant / Owner: GE TECHNOLOGY DEVELOPMENT, INC.,	 	ONE INDEPENDENCE WAY (US)
:Inventors:  	JOEL WALTER ZDEPSKI

:International Patent Classification: H04N 7/56	. . . . Synchronising systems therefor	2006.01	30 Apr 2010
:Priority: UNITED STATES OF AMERICA (US) 	13 May 1993	060,924
:Abstract: APPARATUS FOR DEVELOPING SYNCHRONIZATION OF AN INTERMEDIATE LAYER OF SIGNAL SUCH AS THE TRANSPORT OR MULTIPLEX LAYER OF A MULTI-LAYERED COMPRESSED VIDEO SIGNAL, INCLUDES AT THE ENCODING END OF THE SYSTEM APPARATUS (13, 23, 25) FOR INCLUDING A TIME STAMP REFERENCE, SUCH AS A COUNT VALUE FROM A MODULO K COUNTER (23), AND PROVISION FOR A DIFFERENTIAL TIME STAMP RELATED TO TIME STAMPS OF A FURTHER COMPRESSED SIGNAL OR DIFFERENTIAL TRANSIT TIMES OF RESPECTIVE TRANSPORT PACKETS. FLAGS ARE INCLUDED IN TRANSPORT PACKETS OF SIGNAL TO INDICATE THE PRESENCE OR ABSENCE OF THE TIME STAMPS OR DIFFERENTIAL TIME STAMPS. AT THE RECEIVING END OF THE SYSTEM, CIRCUITRY EXAMINES RESPECTIVE PACKETS FOR THE CONDITION OF THE FLAGS TO LOCATE SPECIFIC INFORMATION SUCH AS THE TIME STAMPS AND DIFFERENTIAL TIME STAMPS. A COUNTER (27) IS RESPONSIVE TO A CONTROLLED RECEIVER CLOCK SIGNAL AND THE COUNT VALUE OF THIS COUNTER IS SAMPLED AT THE ARRIVAL OF TRANSPORT PACKETS INCLUDING A FLAG INDICATING THE PRESENCE OF A TIME STAMP. CIRCUITRY IS DESCRIBED FOR USING THE TIME STAMPS AND THE SAMPLED COUNT VALUES OF THE RECEIVER COUNTER (36) TO PROVIDE A SIGNAL FOR CONTROLLING THE FREQUENCY OF THE RECEIVER CLOCK SIGNAL.FIGURE 3

:Number of Claims: 4
:Number of pages of claims: 4
:Number of pages of drawing: 5
:Number of pages of full specification: 9


===========
MY-128994-A
===========

:Title: APPARATUS FOR FORMATTING A DIGITAL SIGNAL TO INCLUDE MULTIPLE TIME STAMPS FOR SYSTEM SYNCHRONIZATION
:Client Reference: PMC/JKS/192SG1284
:Received Date: 17 May 2001
:Application Number: PI 20012336
:Grant Date: 30 Mar 2007
:OPI Date: 13 Nov 1994
:Filing Date: 27 Apr 1994
:Expiration Date: 30 Mar 2022
:Renewal Due Date:   Please be aware to renew your patent before this date is reached, or else your patent will become expired. 29 Mar 2020
:Agent: PATRICK MIRANDAH
:Applicant / Owner: GE TECHNOLOGY DEVELOPMENT, INC.,	 	ONE INDEPENDENCE WAY (US)
:Inventors:  	JOEL WALTER ZDEPSKI

:International Patent Classification: H04N 7/13	. Systems for the transmission of television signals using pulse code modulation [3,4]#	5	27 Apr 1994
:Priority: UNITED STATES OF AMERICA (US) 	13 May 1993	060,924	 
:Abstract: APPARATUS FOR DEVELOPING SYNCHRONIZATION OF AN INTERMEDIATE LAYER OF SIGNAL SUCH AS THE TRANSPORT OR MULTIPLEX LAYER OF A MULTI-LAYERED COMPRESSED VIDEO SIGNAL, INCLUDES AT THE ENCODING END OF THE SYSTEM APPARATUS (13, 23, 25) FOR INCLUDING A TIME STAMP REFERENCE, SUCH AS A COUNT VALUE FROM A MODULO K COUNTER (23), AND PROVISION FOR A DIFFERENTIAL TIME STAMP RELATED TO TIME STAMPS OF A FURTHER COMPRESSED SIGNAL OR DIFFERENTIAL TRANSIT TIMES OF RESPECTIVE TRANSPORT PACKETS. FLAGS ARE INCLUDED IN TRANSPORT PACKETS OF SIGNAL TO INDICATE THE PRESENCE OR ABSENCE OF THE TIME STAMPS OR DIFFERENTIAL TIME STAMPS. AT THE RECEIVING END OF THE SYSTEM, CIRCUITRY EXAMINES RESPECTIVE PACKETS FOR THE CONDITION OF THE FLAGS TO LOCATE SPECIFIC INFORMATION SUCH AS THE TIME STAMPS AND DIFFERENTIAL TIME STAMPS. A COUNTER (27) IS RESPONSIVE TO A CONTROLLED RECEIVER CLOCK SIGNAL AND THE COUNT VALUE OF THIS COUNTER IS SAMPLED AT THE ARRIVAL OF TRANSPORT PACKETS INCLUDING A FLAG INDICATING THE PRESENCE OF A TIME STAMP. CIRCUITRY IS DESCRIBED FOR USING THE TIME STAMPS AND THE SAMPLED COUNT VALUES OF THE RECEIVER COUNTER (36) TO PROVIDE A SIGNAL FOR CONTROLLING THE FREQUENCY OF THE RECEIVER CLOCK SIGNAL.(FIG 3)

:Number of Claims: 2
:Number of pages of claims: 2
:Number of pages of drawing: 5
:Number of pages of full specification: 7

=========
News log
=========
2018-02-14 - Last US patent expired (Slashdot post_)

