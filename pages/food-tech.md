<!--
.. title: Food is a technology
.. slug: food-tech
.. date: 2019-12-01 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. datatable: true
-->

My son and I tried some plant-based products (both meat and dairy replacements).  Primarily doing this for the planet/climate.

## Meat

### Ground
**Good**

 * Impossible ($9). Both of us prefer it to our previous ground turkey, by a large margin
 * Kroger's Simple Truth Emerge Plant-Based Grind ($7). Pea based protein - still prefer how Impossible cooks, but was impressed.  Son had seconds.

**OK** Beyond Meat ($9). Good, but still have slight preference for the ground turkey (range from $2-$7) we usually get

**Avoid** MorningStar burrito ground I don't remember the exact one, but it was inedible for both of us.

### Burger
**Avoid** Tried Impossible Burger at Ruby's, not impressed.  In-N-Out accross the street sells better burgers for ~$3. Not worth it.  It was $14.

### Sausauge

**Meh** Field Roast Grain Meat Co.  Breakfast Suasauge.  Apple Maple.  Wasn't bad, but not likley to do again.
**Avoid** Beyond Meat Sausauge ($9). Cooked like Kielbasa (may not be recommended). We both ate it, but wouldn't do it again. Described as "rotted chicken nuggets".

### Asian?
**Just for me** gardein sweet and source porkless bites. Delicious for me, to spicy for my son.

### Deli style
**Meh** Quorn Meatless Turkey-Style Deli Slices. Wouldn't do a again, but wasn't bad.

### Crab Cakes
**Good** Trader Joe's Vegan Jackfruit Cakes

## Dairy

### Sour Cream
**Good** Tofutti Better Than Sour Cream $3.99 (soy based)

**OK** Simple Truth Organic™ Non-Dairy Sour Cream $2.79 ($1 coupon) - butter bean based, caused some minor digestive issues.  Tastes great.

### Cream Cheese

**Good** Daiya Strawberry Cream Cheese Style Spread  4.49 (50c coupon)

**OK** Simple Truth Organic™ Plant-Based Plain Cream Cheese Alternative butter bean based, caused some minor digestive issues. Tastes great.

## Snacks
**Great** From the Ground Up Cailiflower Stalks - Cheddar Flavor (Fully Vegan)
