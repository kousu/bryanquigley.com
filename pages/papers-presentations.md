### Starship Congress 2015 - Software on a Manned Interstellar Expedition

 * Presentation [https://docs.google.com/presentation/d/1RZEQCyCmX_mfV4VCjQFVyImHMQB4F-7m_TsAVWg6OHM/edit?usp=sharing](https://docs.google.com/presentation/d/1RZEQCyCmX_mfV4VCjQFVyImHMQB4F-7m_TsAVWg6OHM/edit?usp=sharing)
 * [Draft Paper](papers/software-on-a-manned-interstellar-expedition.html)
 * Youtube [https://www.youtube.com/watch?v=rqBKqNz1P4U](https://www.youtube.com/watch?v=rqBKqNz1P4U)

### Ubuntu
 * [Monthly Update Cadence proposal](papers/ubuntu-monthly-update-cadence.html)
 * [Proposals to drop i386](papers/ubuntu-drop-i386.html)

### Trackers
 * [2020 Presidential Candidates](papers/2020-presidential-websites-tracker.html)
 * [Food is a technology](food-tech.html)
 * [MPEG-2 Patents](mpeg2-patent-tracker.html)

### Other

 * [History](random-blog-history.html)
 * [Policy](policy.html)
 * [Policy](policy.html)
