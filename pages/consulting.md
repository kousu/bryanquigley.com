<!--
.. title: Consulting and Services
.. slug: consulting
.. date: 2020-07-26 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

# Static Websites $199

 * Basic 3 short page static website
 * Hosted for free by [Github](https://pages.github.com/) or Gitlab pages. Note their [terms of service](https://help.github.com/en/github/working-with-github-pages/about-github-pages) do apply
 * Send in a format convenient for you
 * No need to worry about website security
 * You can make simple changes via the Github interface
 * If you don't want to make any changes it requires *no* Wordpress like maintenance (or costs)
  
How it works:

 0. Quick call or email to determine if we are a good fit
 0. You send what you want the website to say and pick a general theme
 0. I set up new Github account for you and create the site
 0. You review the site and confirm you like the new site setup - If you don't like it we have another discussion and determine how to proceed
 0. With payment I transfer the Github account and control to you
 0. I help you switch the site to be live on your domain with HTTPS

 
# Open Source and SaaS Consulting $99/hour

I have over 15 years general system administration experience and 10 years focused on open source software.  Got a question or bug that you need help with?  Reach out!
 
 * OS: Ubuntu/Debian Linux and general Linux.
 * Monitoring: Experience with Nagios, Grafana, and various SaaS.
 * Containers and Virtualization: KVM and LXD
 * Kernel: Need help with understanding if an upgrade will help fix the issue you are having?
 * Organizational: Need help making a security policy or other technical policy?
 * Security: Want best practices? Need hardening of your systems or setup?
 * Product search: Have a need and want to understand what products would best solve your issue?
 * SaaS: G Suite, Files.com, Github, Gitlab
 
Email me at consulting@bryanquigley.com or use the form below:
 
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSftJ-lJUQ71akJAKL1Fv2YEfDq4ozUaCi6iYo4UDq7V4SOo4A/viewform?embedded=true" width="800" height="960" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
